﻿
var ElectroConfig = 
{
	"sens":
	[
		{
			"format": "%s кВт ч",
			"addr": "27",
			"re": "\\d+\[\.,]?\\d{0,2}"
		},
		{
			"format": "%s кВт",
			"addr": "70",
			"re": "\\d+\[\.,]?\\d{0,2}"
		}
	],
	"btn":
	[
		{
			"name": null,
			"btn_addr": "172",
			"ind_addr": "48"
		},
		{
			"name": null,
			"btn_addr": "173",
			"ind_addr": "49"
		},
		{
			"name": null,
			"btn_addr": "174",
			"ind_addr": "50"
		}
	]
};