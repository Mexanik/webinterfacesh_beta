﻿
var AlarmConfig = 
[
	{
		"name":	"Прихожая",
		"objects":
		[
			{
				"name": "Входная дверь",
				"type": "long",
				"unline": true,
				"addr": 49
			},
			{
				"name": "Замок электрический",
				"type": "long",
				"unline": true,
				"addr": 50
			},
			{
				"name": "Замок механический",
				"type": "long",
				"unline": true,
				"addr": 51
			},
			{
				"name": "Датчик движения",
				"type": "long",
				"unline": true,
				"addr": 52
			}
		]
	},
	{
		"name":	"Датчики протечки",
		"objects":
		[
			{
				"name": "Протечка на кухне",
				"type": "long",
				"unline": true,
				"addr": "66",
				"reset_addr": "210"
			},
			{
				"name": "Протечка в сан.узле",
				"type": "long",
				"addr": "67",
				"reset_addr": "212"
			},
			{
				"name": "Протечка в ванной",
				"type": "long",
				"addr": "68",
				"reset_addr": "211"
			}
		]
	},
	{
		"name":	"Датчики света",
		"objects":
		[
			{
				"name": "Большая комната",
				"type": "long",
				"unline": true,
				"addr": "71",
				"reset_addr": "210"
			},
			{
				"name": "Маленькая комната",
				"type": "long",
				"addr": "72",
				"reset_addr": "212"
			},
			{
				"name": "Прихожая",
				"type": "long",
				"addr": "73",
				"reset_addr": "211"
			},
			{
				"name": "Кухня",
				"type": "long",
				"unline": true,
				"addr": "74",
				"reset_addr": "210"
			},
			{
				"name": "Сан-узел",
				"type": "long",
				"addr": "75",
				"reset_addr": "212"
			},
			{
				"name": "Балкон",
				"type": "long",
				"addr": "76",
				"reset_addr": "211"
			}
		]
	},
	{
		"name":	"Датчики газа",
		"objects":
		[
			{
				"name": "Утечка на кухне",
				"type": "long",
				"unline": true,
				"addr": "77",
				"reset_addr": "210"
			}
		]
	}
];

