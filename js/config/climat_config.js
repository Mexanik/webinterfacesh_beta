
var ClimatConfig = 
[
	{
		"name":	"Помещения",
		"objects":
		[
			{
				"name": "Детская 1",
				"curr_addr": "3",
				"ind_addr": "4",
				"off_addr": "76",
				"on_addr": "77",
				"type": "t3"
			},
			{
				"name": "Детская 2",
				"curr_addr": "1",
				"ind_addr": "2",
				"on_addr": "74",
				"off_addr": "73",
				"type": "t3"
			},
			{
				"name": "Кабинет",
				"curr_addr": "7",
				"ind_addr": "8",
				"on_addr": "83",
				"off_addr": "82",
				"type": "t3"
			},
			{
				"name": "Кинотеатр",
				"curr_addr": "11",
				"ind_addr": "12",
				"on_addr": "89",
				"off_addr": "88",
				"type": "t3"
			},
			{
				"name": "Кухня",
				"curr_addr": "9",
				"ind_addr": "10",
				"on_addr": "86",
				"off_addr": "85",
				"type": "t3"
			},
			{
				"name": "Спальня",
				"unline": true,
				"curr_addr": "5",
				"ind_addr": "6",
				"on_addr": "80",
				"off_addr": "79",
				"type": "t3"
			}
		]
	},
	{
		"name":	"Теплые полы",
		"objects":
		[
			{
				"name": "Коллектор пола",
				"curr_addr": "93",
				"targ_addr": "94",
				"minus_addr": "186",
				"plus_addr": "187",
				"off_addr": "185",
				"auto_addr": "169",
				"on_addr": "170",
				"ind_addr": "38", 
				"type": "t2"
			},
			{
				"name": "Кинотеатр",
				"curr_addr": "21",
				"ind_addr": "22",
				"on_addr": "104",
				"off_addr": "103",
				"type": "t3"
			},
			{
				"name": "Кухня",
				"curr_addr": "19",
				"ind_addr": "20",
				"on_addr": "101",
				"off_addr": "100",
				"type": "t3"
			},
			{
				"name": "Прихожая",
				"curr_addr": "15",
				"ind_addr": "16",
				"on_addr": "95",
				"off_addr": "94",
				"type": "t3"
			},
			{
				"name": "Сан. узел",
				"curr_addr": "17",
				"ind_addr": "18",
				"on_addr": "98",
				"off_addr": "97",
				"type": "t3"
			},
			{
				"name": "Гост. сан. узел",
				"unline": true,
				"curr_addr": "13",
				"ind_addr": "14",
				"on_addr": "92",
				"off_addr": "91",
				"type": "t3"
			}
		]
	},
	{
		"name":	"Полотенцесуш.",
		"objects":
		[
/*			{
				"type":	"th",
				"headers":
				[
					{"str": "Заголовок 1", "x": "-150", "y": "0"},
					{"str": "Заголовок 2", "x": "-50", "y": "0"},
					{"str": "Заголовок 3", "x": "50", "y": "0"},
					{"str": "Заголовок 4", "x": "150", "y": "0"}
				]
			},*/
			{
				"name": "Сан. узел",
				"curr_addr": "23",
				"ind_addr": "24",
				"off_addr": "106",
				"on_addr": "107",
				"type": "t3"
			},
			{
				"name": "Гостевой СУ",
				"unline": true,
				"curr_addr": "25",
				"ind_addr": "26",
				"off_addr": "109",
				"on_addr": "110",
				"type": "t3"
			}
		]
	}
];

var ClimatSceneConfig = 
[
/*	{
		"name": "Лето",
		"btn_addr": "170",
		"ind_addr": "46"
	},
	{
		"name": "Зима",
		"btn_addr": "171",
		"ind_addr": "47"
	}*/
];

