﻿
var LightConfig =
[
	{
		"name":	"Прихожая",
		"objects":
		[
			{
				"name": "Потолок",
				"type": "simple",
				"btn_addr": "1",
				"ind_addr": "1"
			},
			{
				"name": "Замок",
				"type": "simple",
				"btn_addr": "26",
				"ind_addr": "26"
			}
		],
		"scene":
		[
		]
	},
	{
		"name":	"Большая",
		"objects":
		[
			{
				"name": "Левый свет",
				"type": "simple",
				"btn_addr": "2",
				"ind_addr": "2"
			},
			{
				"name": "Правый свет",
				"type": "simple_r",
				"btn_addr": "3",
				"ind_addr": "3"
			},
			{
				"name": "Светильник",
				"type": "simple",
				"btn_addr": "4",
				"ind_addr": "4"
			}
		],
		"scene":
		[
			{
				"name": "Включить все",
				"btn_addr": "5",
				"ind_addr": "5"
			},
			{
				"name": "Выключить все",
				"btn_addr": "6",
				"ind_addr": "6"
			}
		]
	},
	{
		"name":	"Маленькая",
		"objects":
		[
			{
				"name": "Люстра",
				"type": "simple",
				"btn_addr": "7",
				"ind_addr": "7"
			},
			{
				"name": "Лампа",
				"type": "simple",
				"btn_addr": "8",
				"ind_addr": "8"
			},
			{
				"name": "Закрыть жалюзи",
				"type": "fan",
				"btn_addr": "27",
				"ind_addr": "27"
			},
			{
				"name": "Открыть жалюзи",
				"type": "fan",
				"btn_addr": "28",
				"ind_addr": "28"
			}
		],
		"scene":
		[
			{
				"name": "Включить все",
				"btn_addr": "9",
				"ind_addr": "9"
			},
			{
				"name": "Выключить все",
				"btn_addr": "10",
				"ind_addr": "10"
			}
		]
	},
	{
		"name":	"Кухня",
		"objects":
		[
			{
				"name": "Люстра",
				"type": "simple",
				"btn_addr": "11",
				"ind_addr": "11"
			},
			{
				"name": "Подсветка",
				"type": "simple",
				"btn_addr": "12",
				"ind_addr": "12"
			},
			{
				"name": "Вентиляция",
				"type": "fan",
				"btn_addr": "13",
				"ind_addr": "13"
			}
		],
		"scene":
		[
			{
				"name": "Включить все",
				"btn_addr": "14",
				"ind_addr": "14"
			},
			{
				"name": "Выключить все",
				"btn_addr": "15",
				"ind_addr": "15"
			}
		]
	},
	{
		"name":	"Сан-узел",
		"objects":
		[
			{
				"name": "Свет в ванной",
				"type": "simple",
				"btn_addr": "16",
				"ind_addr": "16"
			},
			{
				"name": "Свет в туалете",
				"type": "simple",
				"btn_addr": "17",
				"ind_addr": "17"
			},
			{
				"name": "Вентиляция СУ",
				"type": "fan",
				"btn_addr": "18",
				"ind_addr": "18"
			}
		],
		"scene":
		[
			{
				"name": "Включить все",
				"btn_addr": "19",
				"ind_addr": "19"
			},
			{
				"name": "Выключить все",
				"btn_addr": "20",
				"ind_addr": "20"
			}
		]
	},
	{
		"name":	"Балкон",
		"objects":
		[
			{
				"name": "Основной свет",
				"type": "simple",
				"unline": true,
				"btn_addr": "21",
				"ind_addr": "21"
			},
			{
				"name": "Подсветка",
				"type": "simple",
				"btn_addr": "22",
				"ind_addr": "22"
			},
			{
				"name": "Вентиляция",
				"type": "fan",
				"btn_addr": "23",
				"ind_addr": "23"
			}
		],
		"scene":
		[
			{
				"name": "Включить все",
				"btn_addr": "24",
				"ind_addr": "24"
			},
			{
				"name": "Выключить все",
				"btn_addr": "25",
				"ind_addr": "25"
			}
		]
	}
];






