﻿
var AlarmConfig = 
[
	{
		"name":	"Все датчики",
		"objects":
		[
			{
				"name": "Входная дверь",
				"type": "small_l",
				"unline": true,
				"addr": 49
			},
			{
				"name": "Дверь лоджии",
				"type": "small_m",
				"addr": 50
			},
			{
				"name": "Окно спальни",
				"type": "small_r",
				"addr": 60
			},
			{
				"name": "Окно детской1",
				"type": "small_l",
				"unline": true,
				"addr": 61
			},
			{
				"name": "Окно детской2",
				"type": "small_m",
				"addr": 62
			},
			{
				"name": "Окно кабинета",
				"type": "small_r",
				"addr": 59
			},
			{
				"name": "Окно кинотеатра",
				"type": "small_l",
				"addr": 63
			},
			{
				"name": "У входной двери",
				"type": "small_l",
				"unline": true,
				"addr": 51
			},
			{
				"name": "У гардероба",
				"type": "small_m",
				"addr": 56
			},
			{
				"name": "Кабинет",
				"type": "small_r",
				"addr": 52
			},
			{
				"name": "Спальня",
				"type": "small_l",
				"unline": true,
				"addr": 53
			},
			{
				"name": "Детская1",
				"type": "small_m",
				"addr": 54
			},
			{
				"name": "Детская2",
				"type": "small_r",
				"addr": 55
			},
			{
				"name": "Кухня",
				"type": "small_l",
				"addr": 57
			},
			{
				"name": "Кинотеатр",
				"type": "small_m",
				"addr": 58
			},
			{
				"name": "Сейф",
				"type": "small_r",
				"addr": 64
			},
			{
				"name": "ПД Гардероб",
				"type": "small_l",
				"unline": true,
				"addr": "48",
				"reset_addr": "114"
			},
			{
				"name": "ПД Детские",
				"type": "small_m",
				"addr": "46",
				"reset_addr": "112"
			},
			{
				"name": "ПД Коридор",
				"type": "small_r",
				"addr": "47",
				"reset_addr": "113"
			},
			{
				"name": "ПД Кухня",
				"type": "small_l",
				"addr": "92",
				"reset_addr": "149"
			},
			{
				"name": "Вода в кухне",
				"type": "small_l",
				"unline": true,
				"addr": "66"
			},
			{
				"name": "Вода в сан.узле",
				"type": "small_m",
				"addr": "68"
			},
			{
				"name": "Вода в гост. СУ",
				"type": "small_r",
				"addr": "67"
			},
			{
				"name": "Газ в кухне",
				"type": "small_l",
				"unline": true,
				"addr": "69"
			}
		]
	},
	{
		"name":	"Внешний периметр",
		"objects":
		[
			{
				"name": "Входная дверь",
				"type": "long",
				"addr": 49,
				"reset_addr": 193
			},
			{
				"name": "Дверь лоджии",
				"type": "long",
				"addr": 50,
				"reset_addr": 194
			},
			{
				"name": "Окно спальни",
				"type": "long",
				"addr": 60,
				"reset_addr": 204
			},
			{
				"name": "Окно детской1",
				"type": "long",
				"addr": 61,
				"reset_addr": 205
			},
			{
				"name": "Окно детской2",
				"type": "long",
				"addr": 62,
				"reset_addr": 206
			},
			{
				"name": "Окно кабинета",
				"type": "long",
				"addr": 59,
				"reset_addr": 208
			},
			{
				"name": "Окно кинотеатра",
				"type": "long",
				"unline": true,
				"addr": 63,
				"reset_addr": 207
			}
		]
	},
	{
		"name":	"Помещения 1",
		"objects":
		[
			{
				"name": "Коридор у входной двери",
				"type": "long",
				"addr": 51,
				"reset_addr": 195
			},
			{
				"name": "Коридор у гардероба",
				"type": "long",
				"addr": 56,
				"reset_addr": 200
			},
			{
				"name": "Кухня",
				"type": "long",
				"addr": 57,
				"reset_addr": 201
			},
			{
				"name": "Кинотеатр",
				"type": "long",
				"unline": true,
				"addr": 58,
				"reset_addr": 202
			}
		]
	},{
		"name":	"Помещения 2",
		"objects":
		[
			{
				"name": "Кабинет",
				"type": "long",
				"addr": 52,
				"reset_addr": 196
			},
			{
				"name": "Сейф",
				"type": "long",
				"addr": 64,
				"reset_addr": 208
			},
			{
				"name": "Спальня",
				"type": "long",
				"addr": 53,
				"reset_addr": 197
			},
			{
				"name": "Детская1",
				"type": "long",
				"addr": 54,
				"reset_addr": 198
			},
			{
				"name": "Детская2",
				"type": "long",
				"unline": true,
				"addr": 55,
				"reset_addr": 199
			}
		]
	},
	{
		"name":	"Пожарные датчики",
		"objects":
		[
			{
				"name": "Гардероб",
				"type": "long",
				"addr": "48",
				"reset_addr": "114"
			},
			{
				"name": "Детские",
				"type": "long",
				"addr": "46",
				"reset_addr": "112"
			},
			{
				"name": "Коридор",
				"type": "long",
				"addr": "47",
				"reset_addr": "113"
			},
			{
				"name": "Кухня",
				"type": "long",
				"unline": true,
				"addr": "92",
				"reset_addr": "149"
			}
		]
	},
	{
		"name":	"Датчики утечки",
		"objects":
		[
			{
				"name": "Протечка в кухне",
				"type": "long",
				"addr": "66",
				"reset_addr": "210"
			},
			{
				"name": "Протечка в сан.узле",
				"type": "long",
				"addr": "68",
				"reset_addr": "212"
			},
			{
				"name": "Протечка в гост. сан.узле",
				"type": "long",
				"addr": "67",
				"reset_addr": "211"
			},
			{
				"name": "Утечка газа в кухне",
				"type": "long",
				"unline": true,
				"addr": "69",
				"reset_addr": "213"
			}
		]
	}
];

