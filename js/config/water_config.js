﻿
var WaterConfig =
[
	{
		"name": "Показания счетчика воды",
		"type": "textout",
		"format": "%d м" + String.fromCharCode(179),
		"addr": "28"
	},
	{
		"name": "Давление воды 1",
		"type": "textout",
		"format": "%D атм.",
		"addr": "36"
	},
	{
		"name": "Давление воды 2",
		"type": "textout",
		"format": "%D атм.",
		"addr": "37"
	},
	{
		"name": "Клапан воды",
		"type": "valve",
		"format": " МПа",
		"addr": "86"
	},
	{
		"name": "Пол в прихожей",
		"type": "valve_r",
		"addr": "87"
	},
	{
		"name": "Пол в кухне",
		"type": "valve",
		"addr": "89"
	},
	{
		"name": "Пол в кинотеатре",
		"type": "valve_r",
		"addr": "90"
	},
	{
		"name": "Пол в сан. узле",
		"type": "valve",
		"addr": "88",
		"unline": true
	},
	{
		"name": "Пол в гост. СУ",
		"type": "valve_r",
		"addr": "92"
	}
];

WaterConfigExt =
{
	"btn_addr":	75,
	"btn_text": "Промывка фильтра"
};