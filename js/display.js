var DisplayInterfaceDescr =
{
	"name": "g",
	"child":
	[
		{
			"name": "rect",
			"attr":
			{
				"x": "0",
				"y": "0",
				"width": "0",
				"height": "0",
				"rx": "15",
				"stroke": "none",
				"stroke-width": "0",
				"fill": "orange",
				"fill-opacity": "0",
				"class": "border_rect"
			}
		},
		{
			"name": "svg",
			"attr":
			{
				"x": "10",
				"y": "10",
				"width": "30",
				"height": "30",
				"viewBox": "0 0 30 30",
				"preserveAspectRatio": "none",
				"class": "disp_box"
			},
			"child":
			[
				{
					"name": "rect",
					"attr":
					{
						"width": "1000",
						"height": "30",
						"fill-opacity": "0.0"
					}
				}
			]
		}
	]
};

function createDisplayControl(addr_offset, dd)
{
	var new_control = createMyElement(DisplayInterfaceDescr);	
	new_control.border_rect = getElementByClass(new_control, "border_rect");
	new_control.disp_box = getElementByClass(new_control, "disp_box");
	
	new_control.dd = dd;
	
	new_control.moveMode = "all";
	
	new_control.moveX = function(offsetX)
	{
		if(new_control.moveMode != "all")
			if(new_control.moveMode != "pos")return;
			
		var x = this.border_rect.getAttribute("x");
		this.border_rect.setAttribute("x", parseInt(x) + offsetX);
		this.disp_box.setAttribute("x", parseInt(x) + offsetX + 10);
	};
	new_control.moveY = function(offsetY)
	{
		if(new_control.moveMode != "all")
			if(new_control.moveMode != "pos")return;
			
		var y = this.border_rect.getAttribute("y");
		this.border_rect.setAttribute("y", parseInt(y) + offsetY);
		this.disp_box.setAttribute("y", parseInt(y) + offsetY + 10);
	};
	
	new_control.moveW = function(offsetWidth)
	{
		if(new_control.moveMode != "all")
			if(new_control.moveMode != "w")return;
		
		var width = parseInt(this.border_rect.getAttribute("width"));
		//alert(width);
		if(parseInt(width) + offsetWidth < (20 + 20))return;
		var height = parseInt(this.border_rect.getAttribute("height"));
		//alert(height);
		this.border_rect.setAttribute("width", parseInt(width) + offsetWidth);
		this.disp_box.setAttribute("width", parseInt(width) + offsetWidth - 20);
		this.disp_box.setAttribute("viewBox", "0 0 " + parseInt(width + offsetWidth - 20) + " " + (height));
	};
	new_control.moveH = function(offsetHeight)
	{
		if(new_control.moveMode != "all")
			if(new_control.moveMode != "h")return;
			
		var width = parseInt(this.border_rect.getAttribute("width"));
		var height = parseInt(this.border_rect.getAttribute("height"));
		if(parseInt(height) + offsetHeight < (20 + 20))return;
		this.border_rect.setAttribute("height", parseInt(height) + offsetHeight);
		this.disp_box.setAttribute("height", parseInt(height) + offsetHeight - 20);
		this.disp_box.setAttribute("viewBox", "0 0 " + (width) + " " + parseInt(height + offsetHeight - 20));
	};
	
	new_control.border_rect.onmousedown = function()
	{
		if(new_control.dd == null)return;
	
		new_control.dd.on(new_control);
		var x = parseInt(new_control.border_rect.getAttribute("x"));
		var y = parseInt(new_control.border_rect.getAttribute("y"));
		var width = parseInt(new_control.border_rect.getAttribute("width"));
		var height = parseInt(new_control.border_rect.getAttribute("height"));
		var strokeW = parseInt(new_control.border_rect.getAttribute("stroke-width"));
		
		if(((x - strokeW) <= event.x) && (event.x <= (x + strokeW)))
		{
			new_control.moveMode = "pos";
		}
		else if(((y - strokeW) <= event.y) && (event.y <= (y + strokeW)))
		{
			new_control.moveMode = "pos";
		}
		else if(((x + width - strokeW) <= event.x) && (event.x <= (x + width + strokeW)))
		{
			new_control.moveMode = "w";
		}
		else if(((y + height - strokeW) <= event.y) && (event.y <= (y + height + strokeW)))
		{
			new_control.moveMode = "h";
		}
	};
	
	new_control.curr_text = "";
	new_control.charCount = 30;
	new_control.print = function(text, charCount)
	{
		this.curr_text = text;
	
		var chArray = this.disp_box.childNodes;
		
		while(chArray.length > 1)
			this.disp_box.removeChild(chArray[1]);
			
		var index = 0;
		var spacePos = 0;
		var startPos = 0;
		var line = 1;
		while(true)
		{
			if(startPos >= text.length)break;
			spacePos = startPos + this.charCount;
			for(index = startPos; index < text.length; index++)
			{
				if(index - startPos == this.charCount)
				{
					//startPos = spacePos;
					break;
				}
				if(text[index] == " ")
				{
					spacePos = index;
				}
			}
			if(index == text.length)
			{
				spacePos = text.length;
			}
			
			var new_text = document.createElementNS("http://www.w3.org/2000/svg", "text");
			new_text.setAttribute("x", 0);
			new_text.setAttribute("y", line * 30);
			new_text.setAttribute("font-family", "Courier");
			new_text.setAttribute("font-size", "30");
			new_text.setAttribute("font-weight", "bolder");
			new_text.setAttribute("fill", "white");
			new_text.appendChild(document.createTextNode(text.substr(startPos, spacePos - startPos)));
			this.disp_box.appendChild(new_text);
			
			line++;
			startPos = spacePos + 1;
		}
	};
	
	new_control.last_line = 0;
	new_control.text_x_position = 0;
	new_control.color = "black";
	new_control.print_plus = function(text, charCount)
	{
		var chArray = this.disp_box.childNodes;
			
		var index = 0;
		var spacePos = 0;
		var startPos = 0;
		var line = this.last_line;
		while(true)
		{
			if(startPos >= text.length)break;
			spacePos = startPos + this.charCount;
			for(index = startPos; index < text.length; index++)
			{
				if(index - startPos == this.charCount)
				{
					//startPos = spacePos;
					break;
				}
				if(text[index] == " ")
				{
					spacePos = index;
				}
			}
			if(index == text.length)
			{
				spacePos = text.length;
			}
			
			/*var new_text = document.createElementNS("http://www.w3.org/2000/svg", "text");
			new_text.setAttribute("x", 0);
			new_text.setAttribute("y", line * 30);
			new_text.setAttribute("font-family", "Courier");
			new_text.setAttribute("font-size", "20");
			new_text.setAttribute("font-weight", "bolder");
			new_text.setAttribute("fill", "white");
			new_text.appendChild(document.createTextNode(text.substr(startPos, spacePos - startPos)));*/
			var new_text = createMyElement(
			{
				"name": "g",
				"attr":
				{
					"transform": "translate(0 " + line*30 + ")"
				},
				"child":
				[
					{
						"name": "text",
						"attr":
						{
							"x": this.text_x_position,
							"y": "30",
							"font-family": "Courier",
							"font-size": "20",
							"font-weight": "bolder",
							"fill": this.color
						},
						"child":
						[
							text.substr(startPos, spacePos - startPos)
						]
					}
				]
			});
			this.disp_box.appendChild(new_text);
			
			line++;
			startPos = spacePos + 1;
		}
		this.last_line = line;
				
		try{
			this.disp_box.childNodes[0].setAttribute("height", (line + 2)*30);
		}
		catch(err)
		{
			alert(err);
		}
	};
	
	new_control.clear = function()
	{
		var chArray = this.disp_box.childNodes;
		
		while(chArray.length > 1)
			this.disp_box.removeChild(chArray[1]);
			
		this.last_line = 0;
	};
	
	new_control.scrollMove = function(pos)
	{
		var viewBox = this.disp_box.getAttribute("viewBox");
		var regEx = /-?\d+\.?\d*/g;
		var ch;
		var coord = [];
		while((ch = regEx.exec(viewBox)) != null)
		{
			coord[coord.length] = ch[0];
		}
			
		var hidden = (this.last_line + 1)*30 - this.disp_box.getAttribute("height");
			
		var new_pos;
		if(hidden < 0)
		{
			new_pos = 0;
		}
		else
		{
			new_pos = (hidden/100)*pos;
		}
			
		//if(Math.abs(new_pos - coord[1]) < 20)return;
	
		var new_tr = "0 " + parseInt(new_pos) + " " + coord[2] + " " + coord[3];
		//this.disp_box.setAttribute("display", "none");
		this.disp_box.setAttribute("viewBox", new_tr);
		//this.disp_box.setAttribute("display", "block");
		//alert(new_tr);
	};
	
	
	new_control.prefix = "";
	
	new_control.setText = function(addr, text)
	{
		/*if(typeof(text) == "object")
		{
			this.setTextExt(addr, text);
			return;			
		}*/
		
		try{
			eval("text = " + text);
			this.setTextExt(addr, text);
			return;
		}
		catch(err)
		{
		
		}
	
		var dt = new Date();
	
		var pref = this.prefix.replace("%h", ("0" + dt.getHours()).substr(-2));
		pref = pref.replace("%m", ("0" + dt.getMinutes()).substr(-2));
		pref = pref.replace("%s", ("0" + dt.getSeconds()).substr(-2));
		pref = pref.replace("%D", ("0" + dt.getDate()).substr(-2));
		pref = pref.replace("%M", ("0" + dt.getMonth()).substr(-2));
		pref = pref.replace("%Y", dt.getYear());
	
		this.print_plus(pref + text, null);		
	}
	
	new_control.setTextExt = function(addr, text_ext)
	{
		var dt = new Date();
	
		try{
		var pref = this.prefix.replace("%h", ("0" + dt.getHours()).substr(-2));
		pref = pref.replace("%m", ("0" + dt.getMinutes()).substr(-2));
		pref = pref.replace("%s", ("0" + dt.getSeconds()).substr(-2));
		pref = pref.replace("%D", ("0" + dt.getDate()).substr(-2));
		pref = pref.replace("%M", ("0" + dt.getMonth()).substr(-2));
		pref = pref.replace("%Y", dt.getYear());
	
		var last_text_x_position = this.text_x_position;
		var last_color = this.color;
		var last_line = this.last_line;
		this.text_x_position = 30;
		if(text_ext.color != undefined)
			this.color = text_ext.color;
		this.print_plus(pref + text_ext.text, null);
		if(this.disp_box.childNodes[last_line + 1] != undefined)
		{
			if(text_ext.icon != undefined)
			{
				if(Icons[text_ext.icon] != undefined)
				{
					var icon = createMyElement(Icons[text_ext.icon]);
					this.disp_box.childNodes[last_line + 1].appendChild(icon);
				}
			}
		}
		this.text_x_position = last_text_x_position;
		this.color = last_color;
		}
		catch(err)
		{
			alert(err);
		}	
	};
	
	return new_control;
};



