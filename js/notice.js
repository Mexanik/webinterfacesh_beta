
function Notice(disp, db, showAllClb, showNewClb)
{
	this.display = disp;
	this.prefix = "";
	
	this.db = db;

	var This = this;
	
	this.all = false;
	
	this.showAllCallback = showAllClb;
	this.showNewCallback = showNewClb;
	
	this.db.transaction(function(tx)
	{
		tx.executeSql(	"select dt, msg, flag from notice_table where flag = 0", [],
						function(tx, result)
						{
							var i;
							for(i = 0; i < result.rows.length; i++)
							{
								var row = result.rows.item(i);
								This.display.setText(20, row.msg);
							}
							if(This.showNewCallback)This.showNewCallback();
						},
						function(tx, error)
						{
							tx.executeSql(	"drop table if exists notice_table", 
											[], 
											null,
											function(tx, error)
											{
												alert(error.message);
											});
											
							tx.executeSql(	"create table notice_table(\
												dt datetime,\
												msg varchar(1024),\
												flag int);",
											[],
											null, 
											function(tx, error)
											{
												alert(error.message);
											});
						});	
	});	
	
};

Notice.prototype.showAll = function()
{
	if(this.all)return;
	this.all = true;

	this.display.clear();

	var This = this;

	this.db.transaction(function(tx)
	{
		tx.executeSql(	"select dt, msg from notice_table", [],
						function(tx, result)
						{
							var i;
							for(i = 0; i < result.rows.length; i++)
							{
								var row = result.rows.item(i);
								This.display.setText(20, row.msg);
							}
							if(This.showAllCallback)This.showAllCallback();
						},
						function(tx, error)
						{
							alert(error.message);
						});
	});
};

Notice.prototype.showNew = function()
{
//	if(!this.all)return;
		
	this.all = false;
	
	this.display.clear();

	var This = this;

	this.db.transaction(function(tx)
	{
		tx.executeSql(	"select dt, msg from notice_table where flag = 0", [],
						function(tx, result)
						{
							var i;
							for(i = 0; i < result.rows.length; i++)
							{
								var row = result.rows.item(i);
								This.display.setText(20, row.msg);
							}
							try{
							if(This.showNewCallback)This.showNewCallback();
							}
							catch(err)
							{
								alert(err);
							}
						},
						function(tx, error)
						{
							alert(error.message);
						});
	});
};

Notice.prototype.mark = function()
{
	this.db.transaction(function(tx)
	{
		tx.executeSql(	"update notice_table set flag = 1", [],
						null,
						function(tx, error)
						{
							alert(error.message);
						});
	});
};

Notice.prototype.setText = function(addr, text_ext)
{
	var dt = new Date();
	
	var text;
	
	if(this.prefix != "")
	{
		var pref = this.prefix.replace("%h", ("0" + dt.getHours()).substr(-2));
		pref = pref.replace("%m", ("0" + dt.getMinutes()).substr(-2));
		pref = pref.replace("%s", ("0" + dt.getSeconds()).substr(-2));
		pref = pref.replace("%D", ("0" + dt.getDate()).substr(-2));
		pref = pref.replace("%M", ("0" + dt.getMonth()).substr(-2));
		pref = pref.replace("%y", dt.getYear());
		pref = pref.replace("%Y", dt.getFullYear());
	
		try
		{
			eval("text = " + text_ext);
			text.text = pref + text.text;
			var str = "{";
			var first = true;
			for(key in text)
			{
				if(first)
				{
					str += "\"" + key + "\":\"" + text[key] + "\"";
					first = false;
				}
				else
				{
					str += ",\"" + key + "\":\"" + text[key] + "\"";
				}
			}
			str += "}";
			text = str;
		}
		catch(err)
		{
			text = pref + text_ext;
		}
	}
	
	this.db.transaction(function(tx)
	{
		var dt_str =	dt.getFullYear() + "-" + dt.getMonth() + "-" + dt.getDate() + " " +
						dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
		tx.executeSql("insert into notice_table(dt, msg, flag) values(?, ?, ?);", [dt_str, text, 0],
						null,
						function(tx, error)
						{
							alert(error.message);
						});
	});
	
	//this.display.setText(addr, text);
};

Notice.prototype.deleteOld = function()
{
	this.db.transaction(function(tx)
	{
		tx.executeSql("	delete from notice_table \
						where dt < datetime('now', '-1 month')",
						[],
						null,
						function(tx, error)
						{
							alert(error.message);
						});
	});
};





