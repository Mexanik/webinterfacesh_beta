
var BtnBaseDescription =
{
	"name":	"g",
	"child":
	[
		{
			"name": "g",
			"attr":
			{
				"class": "ind"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"fill-opacity": "0",
				"width": "10",
				"height": "10",
				"fill": "red",
				"class": "rect"
			}
		}
	]
};

function createImageInput(normal_descr, press_descr, addr, width, height)
{
	try{
	var new_input = createMyElement(BtnBaseDescription);
	}
	catch(err)
	{
		alert(err);
	}
	
	new_input.addr = addr;
	
	new_input.base = new_input;
	
	new_input.down = null;
	new_input.up = null;
	
	new_input.rect = getElementByClass(new_input, "rect");
	new_input.rect.setAttribute("x", -width/2);
	new_input.rect.setAttribute("y", -height/2);
	new_input.rect.setAttribute("width", width);
	new_input.rect.setAttribute("height", height);

	new_input.ind = getElementByClass(new_input, "ind");
	
	new_input.norm = createMyElement(normal_descr);
	//new_input.appendChild(new_input.norm);
	new_input.insertBefore(new_input.norm, new_input.ind);
	//new_input.norm.setAttribute("display", "block");
	new_input.norm.setAttribute("fill-opacity", "1");
	
	new_input.press = createMyElement(press_descr);
	//new_input.appendChild(new_input.press);
	new_input.insertBefore(new_input.press, new_input.ind);
	new_input.press.setAttribute("display", "none");
	//new_input.press.setAttribute("fill-opacity", "0");
	
	
	new_input.setPos = function(x, y)
	{
		this.base.setAttribute("transform", "translate(" + x + " " + y + ")");
	};
	
	new_input.state = false;
	new_input.rect.onmousedown = function()
	{
		main_struct.lowLightScreen.reset()
	
		if(new_input.state)return;
		
		new_input.state = true;
		
		new_input.norm.setAttribute("display", "none");
		new_input.press.setAttribute("display", "block");
		//new_input.norm.setAttribute("fill-opacity", "0");
		//new_input.press.setAttribute("fill-opacity", "1");
		
		var pos = new_input.base.getAttribute("transform");
		var regEx = /-?\d+\.?\d*/g;
		var ch;
		
		var coord = [];
		while((ch = regEx.exec(pos)) != null)
		{
			coord[coord.length] = ch[0];
		}
		
		coord[0] = parseFloat(coord[0]) + 1;
		coord[1] = parseFloat(coord[1]) + 1;
		
		new_input.base.setAttribute("transform", "translate(" + coord[0] + " " + coord[1] + ")");
		
		if(new_input.down != null)new_input.down();
		
		if(new_input.addr)
		{
			//alert(new_input.addr);
			buttonClicked(new_input.addr);
			//changeOutState(new_input.addr, "active");
			//terminalCore.setDI_Active(new_input.addr);
		}
	}
	
	new_input.rect.onmouseup = function()
	{
		if(!new_input.state)return;
		
		new_input.state = false;
		
		new_input.press.setAttribute("display", "none");
		new_input.norm.setAttribute("display", "block");
		//new_input.press.setAttribute("fill-opacity", "0");
		//new_input.norm.setAttribute("fill-opacity", "1");
		
		var pos = new_input.base.getAttribute("transform");
		var regEx = /-?\d+\.?\d*/g;
		var ch;
		
		var coord = [];
		while((ch = regEx.exec(pos)) != null)
		{
			coord[coord.length] = ch[0];
		}
		
		coord[0] = parseFloat(coord[0]) - 1;
		coord[1] = parseFloat(coord[1]) - 1;
		
		new_input.base.setAttribute("transform", "translate(" + coord[0] + " " + coord[1] + ")");
		
		if(new_input.up != null)new_input.up();
		
		if(new_input.addr)
		{
			//terminalCore.setDI_Passive(new_input.addr);
		}
	}
	new_input.rect.onmouseout = new_input.rect.onmouseup;
	
	return new_input;
};