
var SimpleLightBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-37",
				"y": "-37",
				"width": "74",
				"height": "74",
				"href": ["http://www.w3.org/1999/xlink", "img/light/btn_lamp_off.png"]
			}
		}
	]
};

var SimpleLightBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-35.5",
				"y": "-36",
				"width": "71",
				"height": "72",
				"href": ["http://www.w3.org/1999/xlink", "img/light/btn_lamp_on.png"]
			}
		}
	]
};

