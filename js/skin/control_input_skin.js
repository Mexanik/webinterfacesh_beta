
var ControlClimatBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_norm.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-32",
				"width": "63",
				"height": "64",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/climat_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-20",
				"y": "7"
			},
			"child":
			[
				"Климат"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlClimatBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_act.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-32",
				"width": "63",
				"height": "64",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/climat_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-20",
				"y": "7"
			},
			"child":
			[
				"Климат"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-112",
				"y": "-51",
				"width": "225",
				"height": "102",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlWeatherBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_norm.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-36",
				"width": "77",
				"height": "73",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/pogoda_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Погода"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlWeatherBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_act.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-36",
				"width": "77",
				"height": "73",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/pogoda_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Погода"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-112",
				"y": "-51",
				"width": "225",
				"height": "102",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};


var ControlCinemaBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_norm.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-32",
				"width": "62",
				"height": "65",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/tv_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Дом к/т"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlCinemaBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_act.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-32",
				"width": "62",
				"height": "65",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/tv_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Дом к/т"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-112",
				"y": "-51",
				"width": "225",
				"height": "102",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};


var ControlAlarmBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_norm.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-80",
				"y": "-25",
				"width": "40",
				"height": "50",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/ohrana_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Охрана"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlAlarmBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_act.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-80",
				"y": "-25",
				"width": "40",
				"height": "50",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/ohrana_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Охрана"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-112",
				"y": "-51",
				"width": "225",
				"height": "102",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};


var ControlCurtainsBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_norm.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-30",
				"width": "62",
				"height": "60",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/shtory_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Шторы"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlCurtainsBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_act.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-30",
				"width": "62",
				"height": "60",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/shtory_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Шторы"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-112",
				"y": "-51",
				"width": "225",
				"height": "102",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};


var ControlVideoBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_norm.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-29",
				"width": "68",
				"height": "58",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/camera_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Видео"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlVideoBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_act.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-29",
				"width": "68",
				"height": "58",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/camera_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Видео"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-112",
				"y": "-51",
				"width": "225",
				"height": "102",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};


var ControlWaterBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_norm.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-24",
				"width": "72",
				"height": "48",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/voda_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-5",
				"y": "7"
			},
			"child":
			[
				"Вода"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlWaterBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_act.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-24",
				"width": "72",
				"height": "48",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/voda_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-5",
				"y": "7"
			},
			"child":
			[
				"Вода"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-112",
				"y": "-51",
				"width": "225",
				"height": "102",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};


var ControlElectroBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_norm.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-26",
				"width": "53",
				"height": "53",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/electro_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Электр."
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlElectroBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_act.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-26",
				"width": "53",
				"height": "53",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/electro_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Электр."
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-112",
				"y": "-51",
				"width": "225",
				"height": "102",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlLightBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_norm.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-32",
				"width": "44",
				"height": "64",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/svet_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Уст-ва"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var ControlLightBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-110",
				"y": "-49",
				"width": "221",
				"height": "98",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/btn_interface_act.png"]
			}
		},
		{
			"name":	"image",
			"attr":
			{
				"x": "-90",
				"y": "-32",
				"width": "44",
				"height": "64",
				"href": ["http://www.w3.org/1999/xlink", "img_cnt/svet_ico.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "25",
				"fill": "white",
				"x": "-15",
				"y": "7"
			},
			"child":
			[
				"Уст-ва"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-112",
				"y": "-51",
				"width": "225",
				"height": "102",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};
