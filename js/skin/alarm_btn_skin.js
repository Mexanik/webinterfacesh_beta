var SetAlarmBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-50",
				"y": "-22",
				"width": "100",
				"height": "43",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/btn_v2_off.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-42",
				"y": "4"
			},
			"child":
			[
				"Сбросить"
			]
		}
	]
};

var SetAlarmLightBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-50",
				"y": "-22",
				"width": "100",
				"height": "43",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/btn_v2_on.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "black",
				"x": "-42",
				"y": "4"
			},
			"child":
			[
				"Сбросить"
			]
		}
	]
};
