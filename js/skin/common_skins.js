
var PlusLightBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-17.5",
				"y": "-17.5",
				"width": "35",
				"height": "35",
				"href": ["http://www.w3.org/1999/xlink", "img/light/+.png"]
			}
		}
	]
};

var PlusLightBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-17.5",
				"y": "-17.5",
				"width": "35",
				"height": "35",
				"href": ["http://www.w3.org/1999/xlink", "img/light/+.png"]
			}
		}
	]
};


var MinusLightBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-17.5",
				"y": "-17.5",
				"width": "35",
				"height": "35",
				"href": ["http://www.w3.org/1999/xlink", "img/light/-.png"]
			}
		}
	]
};

var MinusLightBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-17.5",
				"y": "-17.5",
				"width": "35",
				"height": "35",
				"href": ["http://www.w3.org/1999/xlink", "img/light/-.png"]
			}
		}
	]
};


var MaxLightBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-43",
				"y": "-15.5",
				"width": "86",
				"height": "31",
				"href": ["http://www.w3.org/1999/xlink", "img/light/btn_off.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "22",
				"fill": "gray",
				"x": "-20",
				"y": "7"
			},
			"child":
			[
				"Вкл."
			]
		}
	]
};

var MaxLightBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-43",
				"y": "-15.5",
				"width": "86",
				"height": "31",
				"href": ["http://www.w3.org/1999/xlink", "img/light/btn_on.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "22",
				"fill": "black",
				"x": "-20",
				"y": "7"
			},
			"child":
			[
				"Вкл."
			]
		}
	]
};


var MinLightBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-43",
				"y": "-15.5",
				"width": "86",
				"height": "31",
				"href": ["http://www.w3.org/1999/xlink", "img/light/btn_off.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "22",
				"fill": "gray",
				"x": "-27",
				"y": "7"
			},
			"child":
			[
				"Откл."
			]
		}
	]
};

var MinLightBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-43",
				"y": "-15.5",
				"width": "86",
				"height": "31",
				"href": ["http://www.w3.org/1999/xlink", "img/light/btn_on.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "22",
				"fill": "black",
				"x": "-27",
				"y": "7"
			},
			"child":
			[
				"Откл."
			]
		}
	]
};


var EmptyBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"rect",
			"attr":
			{
				"x": "-43",
				"y": "-15.5",
				"width": "86",
				"height": "31",
				"fill": "blue",
				"fill-opacity": "0",
				"rx": "5"
			}
		}
	]
};

var EmptyBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"rect",
			"attr":
			{
				"x": "-43",
				"y": "-15.5",
				"width": "86",
				"height": "31",
				"fill": "red",
				"fill-opacity": "0",
				"rx": "5"
			}
		}
	]
};


var CloseBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-50",
				"y": "-22",
				"width": "100",
				"height": "43",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/btn_v2_off.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-36",
				"y": "4"
			},
			"child":
			[
				"Закрыть"
			]
		}
	]
};

var CloseBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-50",
				"y": "-22",
				"width": "100",
				"height": "43",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/btn_v2_on.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "black",
				"x": "-36",
				"y": "4"
			},
			"child":
			[
				"Закрыть"
			]
		}
	]
};


var OpenBtnNormalDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-50",
				"y": "-22",
				"width": "100",
				"height": "43",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/btn_v2_off.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-36",
				"y": "4"
			},
			"child":
			[
				"Открыть"
			]
		}
	]
};

var OpenBtnPressDescription = 
{
	"name": "g",
	"attr":
	{
		"display": "block"
	},
	"child":
	[
		{
			"name":	"image",
			"attr":
			{
				"x": "-50",
				"y": "-22",
				"width": "100",
				"height": "43",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/btn_v2_on.png"]
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "black",
				"x": "-36",
				"y": "4"
			},
			"child":
			[
				"Открыть"
			]
		}
	]
};
