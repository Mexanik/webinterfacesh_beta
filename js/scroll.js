
var ScrollDescr =
{
	"type": "vertical",
	"height": "280",
	"width": "40",
	"background":
	{
		"name": "g",
		"child":
		[
			{
				"name": "path",
				"attr":
				{
					"d": "M0,-140 L0,140",
					"fill": "none",
					"stroke": "white",
					"stroke-width": "3"
				}
			}
		]
	},
	"scrollerW": "32",
	"scrollerH": "65",
	"scroller":
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-16",
					"y": "-32",
					"width": "32",
					"height": "65",
					"href": ["http://www.w3.org/1999/xlink", "img/scroll_btn.png"]
				}
			}
		]
	}
}

function scrollBar(descr, callback)
{
	this.descr = descr;
	this.callback = callback;
	this.free_callback = null;
	
	this.base_el = createMyElement(
	{
		"name": "g"
	});
	
	this.bg = createMyElement(descr.background);
	this.base_el.appendChild(this.bg);
	this.scr = createMyElement(descr.scroller);
	this.base_el.appendChild(this.scr);
	
	this.rect = createMyElement(
	{
		"name": "rect",
		"attr":
		{
			"x": -descr.width/2,
			"y": -descr.height/2 - descr.scrollerH/2,
			"width": descr.width,
			"height": parseInt(descr.height) + parseInt(descr.scrollerH),
			"fill-opacity": "0"
		}
	});
	this.base_el.appendChild(this.rect);
	
	this.scr_rect = createMyElement(
	{
		"name": "rect",
		"attr":
		{
			"x": -descr.scrollerW/2,
			"y": -descr.scrollerH/2,
			"width": descr.scrollerW,
			"height": descr.scrollerH,
			"fill-opacity": "0",
			"fill": "green"
		}
	});
	this.base_el.appendChild(this.scr_rect);
	
	this.last_cursorPos = null;
	this.position;
	
	this.capt = false;
	var This = this;
	this.scr_rect.onmousedown = function()
	{
		if(This.capt)return;
		This.capt = true;
		
		This.scr_rect.setAttribute("display", "none");
		
		This.last_cursorPos = event.y;
	};
	
	this.rect.onmouseup = function()
	{
		if(!This.capt)return;
		This.capt = false;
		
		This.scr_rect.setAttribute("display", "block");
		
		This.free_callback(This.position);
	};
	this.rect.onmouseout = this.rect.onmouseup;
	
	
	this.rect.onmousemove = function()
	{
		if(!This.capt)return;
		
		var offset = event.y - This.last_cursorPos;
		
		var new_pos = (100/This.descr.height) * offset + This.position;
		
		if((new_pos < 0) || (100 < new_pos))return;
		
		This.setScrollPos(new_pos);
		This.last_cursorPos = event.y;
	};
};

scrollBar.prototype.setPos = function(x, y)
{
	this.base_el.setAttribute("transform", "translate(" + x + " " + y + ")");
};

scrollBar.prototype.setScrollPos = function(pos)
{
	this.position = pos;

	var hPos = (this.descr.height/100) * pos - this.descr.height/2;
	
	this.scr_rect.setAttribute("y", hPos - this.descr.scrollerH/2);	
	this.scr.setAttribute("transform", "translate(0 " + hPos + ")");
	
	if(this.callback != null)
		this.callback(pos);
};





