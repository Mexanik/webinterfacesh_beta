function createMyElement(descr)
{
	var new_el = document.createElementNS("http://www.w3.org/2000/svg", descr.name);

	var attrName;
	if(descr.attr != undefined)
	{
		for(attrName in descr.attr)
		{
			//alert(attrName + " " + descr.attr[attrName]);
			switch(typeof(descr.attr[attrName]))
			{
				case "string":
				case "number":
					new_el.setAttribute(attrName, descr.attr[attrName]);
					break;
					
				case "object":
					//alert(descr.attr[attrName][0] + " " + attrName + " " + descr.attr[attrName][1]);
					//new_el.setAttributeNS(descr.attr[attrName][0], attrName, descr.attr[attrName][1]);
					new_el.setAttributeNS("http://www.w3.org/1999/xlink", attrName, descr.attr[attrName][1]);
					break;
					
				default:
					//alert(descr.attr[attrName]);
					alert(typeof(descr.attr[attrName]));
					break;
			}
		}
	}
	
	var index;
	var new_child;
	if(descr.child != undefined)
	{
		for(index = 0; index < descr.child.length; index++)
		{
			switch(typeof(descr.child[index]))
			{
				case "object":
					new_child = createMyElement(descr.child[index]);
					break;
				case "string":
					new_child = document.createTextNode(descr.child[index]);
					break;
			}
			new_el.appendChild(new_child);
		}
	}
	
	return new_el;
};

function getElementByClass(par, class_name)
{
	var el = document.evaluate("descendant::*[attribute::class='" + class_name + "']",
								par, 
								null, 
								XPathResult.ANY_TYPE, 
								null).iterateNext();
	return el;
};