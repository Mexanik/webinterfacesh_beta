

function LineChart()
{
	var descr =
	{
		"name": "g",
		"child":
		[
			{
				"name": "rect",
				"attr":
				{
					"x": "-50",
					"y": "-25",
					"width": "100",
					"height": "50",
					"fill": "dimgray",
					"rx": "10",
					"stroke": "white",
					"stroke-width": "4",
					"stroke-opacity": "0.5",
					"class": "bg_rect"
				}
			},
			{
				"name": "g",
				"attr":
				{
					"class": "x_text"
				}
			},
			{
				"name": "g",
				"attr":
				{
					"class": "y_text"
				}
			},
			{
				"name": "svg",
				"attr":
				{
					"x": "-50",
					"y": "-25",
					"width": "100",
					"height": "50",
					"viewBox": "-50 -25 100 50",
					"class": "draw_panel"
				},
				"child":
				[
					/*{
						"name": "rect",
						"attr":
						{
							"x": -1000,
							"y": -1000,
							"width": 2000,
							"height": 2000
						}
					}*/
				]
			}
		]
	};

	this.base_el = createMyElement(descr);
	this.draw_panel = getElementByClass(this.base_el, "draw_panel");	
	this.bg_rect = getElementByClass(this.base_el, "bg_rect");	
	this.x_text = getElementByClass(this.base_el, "x_text");
	this.y_text = getElementByClass(this.base_el, "y_text");
	
	this.pointsArray = [];
	
	this.minX = 0;
	this.maxX = 0;
	this.minY = 0;
	this.maxY = 0;
};

LineChart.prototype.show = function()
{
	this.base_el.setAttribute("display", "block");
};

LineChart.prototype.hide = function()
{
	this.base_el.setAttribute("display", "none");
};

LineChart.prototype.draw = function()
{
	var windowW = this.draw_panel.getAttribute("width");
	var windowH = this.draw_panel.getAttribute("height");
	
	var skaleX = 10;
	var skaleY = 10;
	if(this.maxX - this.minX)
	{
		skaleX = windowW/(this.maxX - this.minX);
	}
	if(this.maxY - this.minY)
	{
		skaleY = windowH/(this.maxY - this.minY);
	}

	var i;
	var path = "";
	if(this.pointsArray.length > 1)		// ���� ����� �� ������
	{
		path = "M" + (this.pointsArray[0].x - this.minX)*skaleX + "," + 
					(windowH - (this.pointsArray[0].y - this.minY)*skaleY) + " ";
	}
	else
	{
		return;
	}
	for(i = 1; i < this.pointsArray.length; i++)
	{
		path += "L" + (this.pointsArray[i].x - this.minX)*skaleX + "," + 
					(windowH - (this.pointsArray[i].y - this.minY)*skaleY) + " ";
	}
	
	//alert(path);
	
	while(this.draw_panel.childNodes.length)
		this.draw_panel.removeChild(this.draw_panel.childNodes[0]);
		
	var polyline = createMyElement(
	{
		"name": "path",
		"attr":
		{
			"d": path,
			"fill": "none",
			"stroke-width": "3",
			"stroke": "red"
		}
	});
	
	this.draw_panel.appendChild(polyline);
};

LineChart.prototype.setPos = function(x, y)
{
	this.base_el.setAttribute("transform", "translate(" + x + " " + y + ")");
};

LineChart.prototype.setSize = function(w, h)
{
	this.draw_panel.setAttribute("x", -w/2 + 50);
	this.draw_panel.setAttribute("y", -h/2 + 10);
	this.draw_panel.setAttribute("width", w - 60);
	this.draw_panel.setAttribute("height", h - 40);
	this.draw_panel.setAttribute("viewBox", "0 0 " + (w - 60) + " " + (h - 40));
	
	this.bg_rect.setAttribute("x", -w/2);
	this.bg_rect.setAttribute("y", -h/2);
	this.bg_rect.setAttribute("width", w);
	this.bg_rect.setAttribute("height", h);
	
	this.draw();
};

LineChart.prototype.dropPoints = function()
{
	this.pointsArray = [];
};

LineChart.prototype.addPoint = function(x, y)
{
	if(this.pointsArray.length == 0)
	{
		this.pointsArray.push({"x": x, "y": y});
		
		this.minX = x;
		this.maxX = x;
		this.minY = y;
		this.maxY = y;
		return;
	}

	if(x < this.minX)this.minX = x;
	if(this.maxX < x)this.maxX = x;
	if(y < this.minY)this.minY = y;
	if(this.maxY < y)this.maxY = y;
	
	var a = 0;

	if(x <= this.pointsArray[a].x)
	{
		this.pointsArray.unshift({"x": x, "y": y});
		return;
	}
	var b = this.pointsArray.length - 1;
	if(this.pointsArray[b].x <= x)
	{
		this.pointsArray.push({"x": x, "y": y});
		return;
	}
	
	var c;
			
	while(true)
	{
		if((b - a) < 2)
		{
			//alert(a + " " + b);
			var tempArray = this.pointsArray.slice(0, a + 1);
			tempArray.push({"x": x, "y": y});
			tempArray = tempArray.concat(this.pointsArray.slice(b));
			this.pointsArray = tempArray;
			return;
		}
		
		c = Math.floor((b + a)/2);
		if(this.pointsArray[c].x < x)
			a = c;
		else
			b = c;
	}
};

LineChart.prototype.drawXAxisText = function(strArray)
{
	var windowW = this.draw_panel.getAttribute("width");
	var windowH = this.draw_panel.getAttribute("height");
	var step = windowW/strArray.length;
	
	var startPosX = parseInt(this.draw_panel.getAttribute("x"));
	var startPosY = parseInt(this.draw_panel.getAttribute("y")) + parseInt(this.draw_panel.getAttribute("height")) + 20;

	while(this.x_text.childNodes.length)
		this.x_text.removeChild(this.x_text.childNodes[0]);
	
	var i;
	for(i = 0; i < strArray.length; i++)
	{
		var x = startPosX + i*step;
		var y = startPosY;
		var new_text = createMyElement(
		{
			"name": "text",
			"attr":
			{
				"x": x,
				"y": y,
				"font-family": "Arial",
				"font-weight": "800",
				"font-size": "20",
				"fill": "white"
			},
			"child":
			[
				strArray[i]
			]
		});

		if(i)
		{
			var new_line = createMyElement(
			{
				"name": "path",
				"attr":
				{
					"d": "M" + x + "," + (y - 20) + " L" + x + "," + (y - 20 - windowH),
					"stroke": "yellow",
					"stroke-width": 0.5,
					"stroke-dasharray": "10,5",
					"stroke-opacity": 1,
					"fill": "none"
				}
			});
			this.x_text.appendChild(new_line);
		}
		
		this.x_text.appendChild(new_text);
	}
};

LineChart.prototype.drawYAxisText = function(strArray)
{
	var windowH = this.draw_panel.getAttribute("height");
	var windowW = this.draw_panel.getAttribute("width");
	var step = windowH/strArray.length;
	
	var startPosX = parseInt(this.draw_panel.getAttribute("x")) - 40;
	var startPosY = parseInt(this.draw_panel.getAttribute("y")) + parseInt(this.draw_panel.getAttribute("height"));

	while(this.y_text.childNodes.length)
		this.y_text.removeChild(this.y_text.childNodes[0]);
	
	var i;
	for(i = 0; i < strArray.length; i++)
	{
		var x = startPosX;
		var y = startPosY - i*step;
		
		var new_text = createMyElement(
		{
			"name": "text",
			"attr":
			{
				"x": x,
				"y": y,
				"font-family": "Arial",
				"font-weight": "800",
				"font-size": "20",
				"fill": "white"
			},
			"child":
			[
				strArray[i]
			]
		});
		
		this.y_text.appendChild(new_text);
		
		if(i)
		{
			var new_line = createMyElement(
			{
				"name": "path",
				"attr":
				{
					"d": "M" + (parseInt(x) + 40) + "," + y + " L" + (parseInt(x) + 40 + parseInt(windowW)) + "," + y,
					"stroke": "yellow",
					"stroke-width": 0.5,
					"stroke-dasharray": "10,5",
					"fill": "none"
				}
			});
			this.y_text.appendChild(new_line);
		}
	}
};





