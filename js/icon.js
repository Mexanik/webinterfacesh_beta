
var Icons =
{
	"icon1":
	{
		"name": "image",
		"attr":
		{
			"x": 0,
			"y": 10,
			"width": 25,
			"height": 25,
			"href": ["http://www.w3.org/1999/xlink", "img/alarm/blue_small.png"]
		}
	},
	"icon2":
	{
		"name": "image",
		"attr":
		{
			"x": 0,
			"y": 10,
			"width": 25,
			"height": 25,
			"href": ["http://www.w3.org/1999/xlink", "img/alarm/red_small.png"]
		}
	},
	"danger":
	{
		"name":"image",
		"attr":
		{
			"x": 0,
			"y": 10,
			"width": 25,
			"height": 25,
			"href": ["http://www.w3.org/1999/xlink", "img/danger.png"]
		}
	},
	"info":
	{
		"name":"image",
		"attr":
		{
			"x": 0,
			"y": 10,
			"width": 25,
			"height": 25,
			"href": ["http://www.w3.org/1999/xlink", "img/info.png"]
		}
	}
};