
function ElectroRegistrater(db, ch)
{
	this.db = db;
	this.chart = ch;
	
	var This = this;
	
	this.chart.base_el.onclick = function()
	{
		This.chart.hide();
	}
	
	this.db.transaction(function(tx)
	{
		tx.executeSql(	"select dt, value from el_reg_tbl \
						 where dt > date('now', '-30 day') \
						 union all \
						 select '2050-01-01 00:00:00', max(d.value) from \
							(select dt, value from el_reg_tbl where dt > date('now', '-30 day')) d \
						 union all \
						 select '2050-01-01 00:00:01', min(d2.value) from \
							(select dt, value from el_reg_tbl where dt > date('now', '-30 day')) d2 \
						 order by dt;", [],
						function(tx, result)
						{
							This.drawChart(result.rows);
						},
						function(tx, error)
						{
							alert(error.message);
							
							tx.executeSql(	"drop table if exists el_reg_tbl;", [],
											null,
											function(tx, error)
											{
												alert(error.message);
											});
											
							tx.executeSql(	"create table el_reg_tbl(\
												dt datetime,\
												value varchar(256));", [],
											null,
											function(tx, error)
											{
												alert(error.message);
											});											
						});
	});
};

ElectroRegistrater.prototype.show = function()
{
	this.chart.show();
	
	var This = this;
	
	this.db.transaction(function(tx)
	{
		tx.executeSql( "select dt, value from el_reg_tbl \
						where dt > date('now', '-30 day') \
						union all \
						select '2050-01-01 00:00:00', max(d.value) from \
							(select dt, value from el_reg_tbl where dt > date('now', '-30 day')) d \
						union all \
						select '2050-01-01 00:00:01', min(d2.value) from \
							(select dt, value from el_reg_tbl where dt > date('now', '-30 day')) d2 \
						order by dt;", [],
						function(tx, result)
						{
							This.drawChart(result.rows);
						},
						function(tx, error)
						{
							alert(error.message);
						});
	});
};

function parseSqliteDate(dt_str)
{
	var regEx = /(\d+)-(\d+)-(\d+)\s(\d+):(\d+):(\d+)/g;
	var res = regEx.exec(dt_str).slice(1);	
	//res[1] -= 1;
	return res;
};

ElectroRegistrater.prototype.drawChart = function(rows)
{
try{
	var i;
	var start_ts;
	var first_ts;
	this.chart.dropPoints();
	if(rows.length > 2)
	{
		var row = rows.item(0);

		var d = parseSqliteDate(row.dt);
		
		start_ts = (new Date()).getTime() - 30*24*3600*1000;
		first_ts = (new Date(d[0], d[1], d[2], d[3], d[4], d[5])).getTime();
		
		if(!isNaN(row.value))
		{
			this.chart.addPoint(0, parseFloat(row.value));
			this.chart.addPoint(first_ts - start_ts, parseFloat(row.value));
			//alert(parseFloat(row.value));
		}
	}
	else
	{
		this.chart.addPoint(0, 0);
		this.chart.addPoint(10, 0);
	}
	for(i = 1; i < rows.length - 3; i++)
	{
		var row = rows.item(i);
		
		var d = parseSqliteDate(row.dt);
		var curr_ts = (new Date(d[0], d[1], d[2], d[3], d[4], d[5])).getTime();
		
		if(!isNaN(row.value))
		{
			this.chart.addPoint(curr_ts - start_ts, parseFloat(row.value));
			//alert(parseFloat(row.value));
		}
	}
	if(rows.length > 2)
	{
		var row = rows.item(rows.length - 3);
		
		var end_ts = (new Date()).getTime();
		var d = parseSqliteDate(row.dt);
		var last_ts = (new Date(d[0], d[1], d[2], d[3], d[4], d[5])).getTime();
		
		if(!isNaN(row.value))
		{
			this.chart.addPoint(last_ts - start_ts, parseFloat(row.value));
			this.chart.addPoint(30*24*3600*1000, parseFloat(row.value));
			//alert(parseFloat(row.value));
		}
		
		var valArray = [];
		var max = rows.item(rows.length - 2).value;
		var min = rows.item(rows.length - 1).value;
		
		var i;
		for(i = 0; i < 5; i++)
		{
			valArray[i] = (((max - min)/5) * i + parseFloat(min)).toFixed(1);
		}
		this.chart.drawYAxisText(valArray);
	}
	
	this.chart.draw();
	var ts = (new Date()).getTime();
	var tsArray = [];
	var i;
	for(i = 0; i < 6; i++)
	{
		var dt = new Date(ts - (5*24*3600*1000)*(6 - i));
		tsArray[i] = dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + ("" + dt.getFullYear()).substr(-2);
	}
	this.chart.drawXAxisText(tsArray);
}
catch(err)
{
	alert(err);
}
};

ElectroRegistrater.prototype.setText = function(addr, text)
{
	var dt = new Date();
	dt = 	dt.getFullYear() + "-" + 
			dt.getMonth() + "-" +
			dt.getDate() + " " +
			dt.getHours() + ":" +
			dt.getMinutes() + ":" +
			dt.getSeconds();
			
	this.db.transaction(function(tx)
	{
		tx.executeSql("	insert into el_reg_tbl(dt, value) \
						values(?, ?);", [dt, text],
						function(tx, result)
						{
							//alert("res");
						},
						function(tx, error)
						{
							alert(error.message);
						});
	});
	
	//this.chart.draw();
};



