
var SceneSelectStyle = 
{
	"step":	40,
	"select_image": "img/light/btn_scene.png",
	"select_width": 190,
	"select_height": 36,
	"select_color": "black",
	"unselect_color": "white",
	"text_pos": "-60"
};

function createSelect(style, items, sel_func)
{
	var new_select = createMyElement(
	{
		"name": "g",
	});
	new_select.current = null;
	new_select.sel_style = style;
	new_select.sel_func = sel_func;
		
	var item_index;
	var new_item;
	for(item_index = 0; item_index < items.length; item_index++)
	{
		new_item = createMyElement(
		{
			"name": "g",
			"child":
			[
				{
					"name": "image",
					"attr":
					{
						"x": -style.select_width/2,
						"y": -style.select_height/2,
						"width": style.select_width,
						"height": style.select_height,
						"display": "none",
						"class": "img",
						"href": ["http://www.w3.org/1999/xlink", style.select_image]
					}
				},
				{
					"name": "text",
					"attr":
					{
						"font-family": "Arial",
						"font-size": "20",
						"fill": style.unselect_color,
						"x": style.text_pos,
						"y": 5,
						"class": "text"
					},
					"child":
					[
						items[item_index].name
					]
				},
				{
					"name": "rect",
					"attr":
					{
						"x": -style.select_width/2,
						"y": -style.select_height/2,
						"width": style.select_width,
						"height": style.select_height,
						"fill": "red",
						"fill-opacity": "0",
						"class": "rect"
					}					
				}
			]
		});
		new_item.name = items[item_index].name;
		new_item.data = items[item_index].data;
		new_item.index = item_index;
		new_item.rect = getElementByClass(new_item, "rect");
		new_item.img = getElementByClass(new_item, "img");
		new_item.text = getElementByClass(new_item, "text");
		new_item.rect.onclick = function()
		{
			var el = event.target.parentNode;
			var select = el.parentNode;
			
			if(select.current != null)
			{
				if(select.current == el)return;
				
				select.current.img.setAttribute("display", "none");
				select.current.text.setAttribute("fill", select.sel_style.unselect_color);
			}
			
			el.img.setAttribute("display", "block");
			el.text.setAttribute("fill", select.sel_style.select_color);
			
			if(select.sel_func != null)select.sel_func(el);
			
			select.current = el;
		};
		new_item.setAttribute("transform", "translate(0 " + (item_index*style.step) + ")");
		new_select.appendChild(new_item);
		if(item_index == 0)
		{
			new_select.current = new_item;
			new_item.text.setAttribute("fill", style.select_color);
			new_item.img.setAttribute("display", "block");
		}
	}
	
	return new_select;
};