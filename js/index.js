
var main_struct;

function appendDevArray(devArray, index, obj)
{
	if(devArray[index] == undefined)
		devArray[index] = [];
		
	devArray[index][devArray[index].length] = obj;
};

function main_init()
{
	main_struct = {};	
	main_struct.screens = {};
	main_struct.outputArray = [];
	main_struct.displayArray = [];
	main_struct.analogOutArray = [];
	
	main_struct.old_clock = "";
	
	main_struct.db = openDatabase("terminal_db", "0.1", "TerminalDB", 1000000);

// Common init
	var common_screen = document.getElementById("smart_home_terminal");
	var common_screen_wp1 = document.getElementById("smart_home_terminal_wp1");
	var common_screen_wp2 = document.getElementById("smart_home_terminal_wp2");
	
	var terminator_eye = createMultiImageOutput([
	{
		"w": 86, 
		"h": 86, 
		"img": "img/green.png"
	},
	{
		"w": 86, 
		"h": 86, 
		"img": "img/yellow.png"
	},
	{
		"w": 86,
		"h": 86,
		"img": "img/red.png"
	}],
	MainConfig.eye.addr);
	terminator_eye.setValue(MainConfig.eye.addr, 0);
	appendDevArray(main_struct.analogOutArray, MainConfig.eye.addr, terminator_eye);

	var terminator_eye_btn = createImageInput({"name": "g"}, {"name": "g"}, null, 86, 86);
	terminator_eye_btn.up = function(){show_info_screen();};
	common_screen_wp2.appendChild(terminator_eye_btn);
	terminator_eye_btn.setPos(960, 55);
	terminator_eye_btn.ind.appendChild(terminator_eye);
	
	var control_btn = createImageInput(ControlBtnNormalDescription, ControlBtnPressDescription, null, 220, 100);
	control_btn.up = function(){switch_screen("control_screen");};
	common_screen_wp2.appendChild(control_btn);
	control_btn.setPos(135, 700);

	var script_btn = createImageInput(ScriptBtnNormalDescription, ScriptBtnPressDescription, null, 220, 100);
	script_btn.up = function(){switch_screen("script_screen");};
	common_screen_wp2.appendChild(script_btn);
	script_btn.setPos(512, 700);
	
	var setting_btn = createImageInput(SettingBtnNormalDescription, SettingBtnPressDescription, null, 220, 100);
	setting_btn.up = function(){switch_screen("option_screen");};
	common_screen_wp2.appendChild(setting_btn);
	setting_btn.setPos(889, 700);
	
	window.setInterval("clockProcess()", 5000);
	
	
// Main	screen init
	var main_screen = document.getElementById("main_screen");
	main_struct.screens["main_screen"] = main_screen;
	
	main_wp = document.getElementById("main_screen_wp1");
	
	var light_btn = createImageInput(ControlLightBtnNormalDescription, ControlLightBtnPressDescription, null, 220, 100);
	light_btn.up = function(){switch_screen("light_screen");};
	main_screen.appendChild(light_btn);
	light_btn.setPos(155, 275);
	
	var climat_btn = createImageInput(ControlWeatherBtnNormalDescription, ControlWeatherBtnPressDescription, null, 220, 100);
	climat_btn.up = function(){switch_screen("weather_screen");};
	main_screen.appendChild(climat_btn);
	climat_btn.setPos(155, 385);
	
	var alarm_btn = createImageInput(ControlAlarmBtnNormalDescription, ControlAlarmBtnPressDescription, null, 220, 100);
	alarm_btn.up = function(){switch_screen("alarm_screen");};
	main_screen.appendChild(alarm_btn);
	alarm_btn.setPos(155, 495);
		
	try{	
	var weather_icon = createMultiImageOutput(
	[
		{
			"w": "100",
			"h": "100",
			"img": "img/weather/sun.png",
			"value": "fair"
		},
		{
			"w": "100",
			"h": "100",
			"img": "img/weather/rain.png",
			"value": "rain"
		},
		{
			"w": "100",
			"h": "100",
			"img": "img/weather/snow.png",
			"value": "snow"
		},
		{
			"w": "100",
			"h": "100",
			"img": "img/weather/cloudly.png",
			"value": "cloud"
		}
	],
	WeatherConfig.ind_addr);
	appendDevArray(main_struct.displayArray, WeatherConfig.ind_addr, weather_icon);
	weather_icon.setAttribute("transform", "translate(590 265)");
	weather_icon.set("fair");
	main_wp.appendChild(weather_icon);		
	
	var outside_term = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "50",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%d C" + String.fromCharCode(176) + ",",
	WeatherConfig.outside_term_addr);
	appendDevArray(main_struct.analogOutArray, WeatherConfig.outside_term_addr, outside_term);
	outside_term.setAttribute("transform", "translate(670 275)");
	outside_term.set("22.2");
	main_wp.appendChild(outside_term);
	
	var outside_damp = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "50",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%D%",
	WeatherConfig.outside_damp_addr);
	appendDevArray(main_struct.analogOutArray, WeatherConfig.outside_damp_addr, outside_damp);
	outside_damp.setAttribute("transform", "translate(860 275)");
	outside_damp.set("32");
	main_wp.appendChild(outside_damp);
	
	var inside_term = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "50",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%d C" + String.fromCharCode(176) + ",",
	WeatherConfig.inside_term_addr);
	appendDevArray(main_struct.analogOutArray, WeatherConfig.inside_term_addr, inside_term);
	inside_term.setAttribute("transform", "translate(670 390)");
	inside_term.set("22.2");
	main_wp.appendChild(inside_term);
	
	var inside_damp = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "50",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%D%",
	WeatherConfig.inside_damp_addr);
	appendDevArray(main_struct.analogOutArray, WeatherConfig.inside_damp_addr, inside_damp);
	inside_damp.setAttribute("transform", "translate(860 390)");
	inside_damp.set("32.3");
	main_wp.appendChild(inside_damp);
	
	var curr_mode1 = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "30",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%s",
	WeatherConfig.mode1_addr);
	appendDevArray(main_struct.displayArray, WeatherConfig.mode1_addr, curr_mode1);
	curr_mode1.setPos(670, 490);
	curr_mode1.set("лето");
	main_wp.appendChild(curr_mode1);
	
	var curr_mode2 = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "30",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%s",
	WeatherConfig.mode2_addr);
	appendDevArray(main_struct.displayArray, WeatherConfig.mode2_addr, curr_mode2);
	curr_mode2.setPos(670, 520);
	curr_mode2.set("я дома");
	main_wp.appendChild(curr_mode2);
	
	}
	catch(err)
	{
		alert(err);
	}
		
// Control	screen init
	var control_screen = document.getElementById("control_screen");
	main_struct.screens["control_screen"] = control_screen;
	
	control_screen_init(control_screen);
	
// Script screen init
	var script_screen = document.getElementById("script_screen");
	main_struct.screens["script_screen"] = script_screen;
	
	script_screen_init(script_screen);
	
// Option screen init
	var option_screen = document.getElementById("option_screen");
	main_struct.screens["option_screen"] = option_screen;
	
	option_screen_init(option_screen);
	
// Light screen init	
	
	var light_screen = document.getElementById("light_screen");
	main_struct.screens["light_screen"] = light_screen;
	
	light_screen_init(light_screen);
	
// Climat screen init
	
	var climat_screen = document.getElementById("climat_screen");
	main_struct.screens["climat_screen"] = climat_screen;
	
	climat_screen_init(climat_screen);
	
// Alarm screen init
	
	var alarm_screen = document.getElementById("alarm_screen");
	main_struct.screens["alarm_screen"] = alarm_screen;
	
	alarm_screen_init(alarm_screen);
	
// Electro screen init
	
	var electro_screen = document.getElementById("electro_screen");
	main_struct.screens["electro_screen"] = electro_screen;
	
	electro_screen_init(electro_screen);
	
// Water screen init
	
	var water_screen = document.getElementById("water_screen");
	main_struct.screens["water_screen"] = water_screen;
	
	water_screen_init(water_screen);

// Weather screen init
	
	var weather_screen = document.getElementById("weather_screen");
	main_struct.screens["weather_screen"] = weather_screen;
	
	weather_screen_init(weather_screen);
	
// Curtains screen init
	
	var curtains_screen = document.getElementById("curtains_screen");
	main_struct.screens["curtains_screen"] = curtains_screen;
	
	curtains_screen_init(curtains_screen);
	
// Main struct init

	main_struct.current_screen = main_screen;
	main_struct.history_screen = [];
	main_struct.history_screen[main_struct.history_screen.length] = main_screen;
	
	//terminalCore.OnDiscrOutputChangeState = "changeOutState";
	
	//terminalCore.OnDisplayShowString = "dispPutStr";
	
	//terminalCore.OnAnalogOutputChangeValue = "setAnalogOutput";	
	
	//terminalCore.OnChangeConnectionState = "changeConnState";
	
	try{
		info_screen_init();
	}
	catch(err)
	{
		alert(err);
	}
	
	try{
	main_struct.watch_dog = {};
	main_struct.watch_dog.timer = window.setInterval("main_struct.watch_dog.proc();", 5000);
	main_struct.watch_dog.count = 0;
	main_struct.watch_dog.proc = function()
	{
		this.count += 5000;
		if(this.count > 20000)
		{
			this.count = 0;
			//alert("связи нету");
			//changeConnState("off");
		}
	};
	}
	catch(err)
	{
		alert(main_struct.watch_dog.timer + " " + err);
	}
	//common.ready();

	
	main_struct.lowLightScreen = {
		timer:	null,
		reset:	function(){
			if(main_struct.lowLightScreen.timer)window.clearInterval(main_struct.lowLightScreen.timer)
			document.getElementById('low_light_screen').setAttribute('display', 'none')
			main_struct.lowLightScreen.timer = window.setInterval(function(){
				window.clearInterval(main_struct.lowLightScreen.timer)
				document.getElementById('low_light_screen').setAttribute('display', 'block')
			}, 15000)
		}
	}
	main_struct.lowLightScreen.timer = window.setInterval(function(){
		window.clearInterval(main_struct.lowLightScreen.timer)
		document.getElementById('low_light_screen').setAttribute('display', 'block')
	}, 15000)
};

function changeConnState(state)
{
	var hide = document.getElementById("conn_state_screen");
	//return;
	
	//alert(state);
	switch(state)
	{
		case "on":
			try{
				main_struct.notice.deleteOld();
			}
			catch(err)
			{}
		
		case "active":
			main_struct.watch_dog.count = 0;
			hide.setAttribute("display", "none");
			break;			
		case "off":
			hide.setAttribute("display", "block");
			break;
	}
};

function changeOutState(addr, state)
{
	var index;
	
	try{
	if(main_struct.outputArray[addr] != undefined)
	{
		for(index = 0; index < main_struct.outputArray[addr].length; index++)
			main_struct.outputArray[addr][index].setState(addr, state);
	}
	}
	catch(err)
	{
		alert(err);
	}
};

function dispPutStr(addr, text)
{
	var index;
	
	try{
	if(main_struct.displayArray[addr] != undefined)
	{
		for(index = 0; index < main_struct.displayArray[addr].length; index++)
			main_struct.displayArray[addr][index].setText(addr, text);
	}
	}
	catch(err)
	{
		alert(err);
	}
};

function setAnalogOutput(addr, value)
{
	var index;
	
	try{
	if(main_struct.analogOutArray[addr] != undefined)
	{
		for(index = 0; index < main_struct.analogOutArray[addr].length; index++)
			main_struct.analogOutArray[addr][index].setText(addr, value);
	}
	}
	catch(err)
	{
		alert(err);
	}
};

function option_screen_init(screen)
{
	var home_btn = createImageInput(HomeBtnNormalDescription, HomeBtnPressDescription, null, 73, 73);
	home_btn.up = function(){switch_home_screen();};
	screen.appendChild(home_btn);
	home_btn.setPos(80, 220);

	var back_btn = createImageInput(BackBtnNormalDescription, BackBtnPressDescription, null, 73, 73);
	back_btn.up = function(){switch_back_screen();};
	screen.appendChild(back_btn);
	back_btn.setPos(180, 220);
	
	var min_btn = createImageInput(
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-74",
					"y": "-23",
					"width": "148",
					"height": "46",
					"href": ["http://www.w3.org/1999/xlink", "img/electro/btn.png"]
				}
			},
			{
				"name": "text",
				"attr":
				{
					"font-family": "Arial",
					"font-size": "28",
					"fill": "white",
					"x": "-62",
					"y": "8"
				},
				"child":
				[
					"Свернуть"
				]
			}
		]
	},
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-74",
					"y": "-23",
					"width": "148",
					"height": "46",
					"href": ["http://www.w3.org/1999/xlink", "img/electro/btn.png"]
				}
			},
			{
				"name": "text",
				"attr":
				{
					"font-family": "Arial",
					"font-size": "28",
					"fill": "white",
					"x": "-62",
					"y": "8"
				},
				"child":
				[
					"Свернуть"
				]
			}
		]
	}, 
	null, 148, 46);
	min_btn.up = function(){common.hide();};
	screen.appendChild(min_btn);
	min_btn.setPos(900, 175);	
};
	
function script_screen_init(screen)
{
	var home_btn = createImageInput(HomeBtnNormalDescription, HomeBtnPressDescription, null, 73, 73);
	home_btn.up = function(){switch_home_screen();};
	screen.appendChild(home_btn);
	home_btn.setPos(80, 220);

	var back_btn = createImageInput(BackBtnNormalDescription, BackBtnPressDescription, null, 73, 73);
	back_btn.up = function(){switch_back_screen();};
	screen.appendChild(back_btn);
	back_btn.setPos(180, 220);

	try{
	var row;
	var col;
	var xPos = 365;
	var yPos = 220;
	var xStep = 252;
	var yStep = 140;
	for(row = 0; row < ScriptConfig.length; row++)
	{
		for(col = 0; col < ScriptConfig[row].length; col++)
		{
			if(ScriptConfig[row][col] == null)
				continue;
				
			var btn = createImageInput(
			{
				"name": "g",
				"child":
				[
					{
						"name": "image",
						"attr":
						{
							"x": "-111",
							"y": "-49",
							"width": "221",
							"height": "98",
							"href": ["http://www.w3.org/1999/xlink", "img/bd_btn_norm.png"]
						}
					},
					{
						"name": "text",
						"attr":
						{
							"font-family": "Arial",
							"font-size": "30",
							"fill": "white",
							"x": "-80",
							"y": "10"
						},
						"child":
						[
							ScriptConfig[row][col].name
						]
					}
				]
			},
			{
				"name": "g",
				"child":
				[
					{
						"name": "image",
						"attr":
						{
							"x": "-113",
							"y": "-51",
							"width": "225",
							"height": "102",
							"href": ["http://www.w3.org/1999/xlink", "img/bd_btn_act.png"]
						}
					},
					{
						"name": "text",
						"attr":
						{
							"font-family": "Arial",
							"font-size": "30",
							"fill": "black",
							"x": "-80",
							"y": "10"
						},
						"child":
						[
							ScriptConfig[row][col].name
						]
					}
				]
			},
			ScriptConfig[row][col].addr, 230, 110);
			btn.setPos(xPos + xStep*col, yPos + yStep*row);
			btn.up = function(){switch_home_screen();};
			
			screen.appendChild(btn);
		}
	}
	}
	catch(err)
	{
		alert(err);
	}
};

function curtains_screen_init(screen)
{
	var home_btn = createImageInput(HomeBtnNormalDescription, HomeBtnPressDescription, null, 73, 73);
	home_btn.up = function(){switch_home_screen();};
	screen.appendChild(home_btn);
	home_btn.setPos(80, 220);

	var back_btn = createImageInput(BackBtnNormalDescription, BackBtnPressDescription, null, 73, 73);
	back_btn.up = function(){switch_back_screen();};
	screen.appendChild(back_btn);
	back_btn.setPos(180, 220);
	
	try{
	build_curtains_screen(CurtainsConfig);
	}
	catch(err)
	{
		alert(err);
	}
};

function weather_screen_init(screen)
{
	var home_btn = createImageInput(HomeBtnNormalDescription, HomeBtnPressDescription, null, 73, 73);
	home_btn.up = function(){switch_home_screen();};
	screen.appendChild(home_btn);
	home_btn.setPos(80, 220);

	var back_btn = createImageInput(BackBtnNormalDescription, BackBtnPressDescription, null, 73, 73);
	back_btn.up = function(){switch_back_screen();};
	screen.appendChild(back_btn);
	back_btn.setPos(180, 220);
	
	try{
	build_weather_screen(WeatherConfig);
	}
	catch(err)
	{
		alert(err);
	}
};

function water_screen_init(screen)
{
	var home_btn = createImageInput(HomeBtnNormalDescription, HomeBtnPressDescription, null, 73, 73);
	home_btn.up = function(){switch_home_screen();};
	screen.appendChild(home_btn);
	home_btn.setPos(80, 220);

	var back_btn = createImageInput(BackBtnNormalDescription, BackBtnPressDescription, null, 73, 73);
	back_btn.up = function(){switch_back_screen();};
	screen.appendChild(back_btn);
	back_btn.setPos(180, 220);
	
	try{
	build_water_screen(WaterConfig);
	}
	catch(err)
	{
		alert(err);
	}
};

function electro_screen_init(screen)
{
	var home_btn = createImageInput(HomeBtnNormalDescription, HomeBtnPressDescription, null, 73, 73);
	home_btn.up = function(){switch_home_screen();};
	screen.appendChild(home_btn);
	home_btn.setPos(80, 220);

	var back_btn = createImageInput(BackBtnNormalDescription, BackBtnPressDescription, null, 73, 73);
	back_btn.up = function(){switch_back_screen();};
	screen.appendChild(back_btn);
	back_btn.setPos(180, 220);
	
	try{
	build_electro_screen(ElectroConfig);
	}
	catch(err)
	{
		alert(err);
	}
};

function alarm_screen_init(screen)
{
	var home_btn = createImageInput(HomeBtnNormalDescription, HomeBtnPressDescription, null, 73, 73);
	home_btn.up = function(){switch_home_screen();};
	screen.appendChild(home_btn);
	home_btn.setPos(80, 220);

	var back_btn = createImageInput(BackBtnNormalDescription, BackBtnPressDescription, null, 73, 73);
	back_btn.up = function(){switch_back_screen();};
	screen.appendChild(back_btn);
	back_btn.setPos(180, 220);
	
	try{
	build_alarm_screen(AlarmConfig);
	}
	catch(err)
	{
		alert(err);
	}
};

function climat_screen_init(screen)
{
	var home_btn = createImageInput(HomeBtnNormalDescription, HomeBtnPressDescription, null, 73, 73);
	home_btn.up = function(){switch_home_screen();};
	screen.appendChild(home_btn);
	home_btn.setPos(80, 220);

	var back_btn = createImageInput(BackBtnNormalDescription, BackBtnPressDescription, null, 73, 73);
	back_btn.up = function(){switch_back_screen();};
	screen.appendChild(back_btn);
	back_btn.setPos(180, 220);
	
	try{
	build_climat_screen(ClimatConfig);
	}
	catch(err)
	{
		alert(err);
	}
};

function light_screen_init(screen)
{
	var home_btn = createImageInput(HomeBtnNormalDescription, HomeBtnPressDescription, null, 73, 73);
	home_btn.up = function(){switch_home_screen();};
	screen.appendChild(home_btn);
	home_btn.setPos(80, 220);

	var back_btn = createImageInput(BackBtnNormalDescription, BackBtnPressDescription, null, 73, 73);
	back_btn.up = function(){switch_back_screen();};
	screen.appendChild(back_btn);
	back_btn.setPos(180, 220);
	
	try{
	build_light_screen(LightConfig);
	}
	catch(err)
	{
		alert(err);
	}
};

function control_screen_init(control_screen)
{
	var home_btn = createImageInput(HomeBtnNormalDescription, HomeBtnPressDescription, null, 73, 73);
	home_btn.up = function(){switch_home_screen();};
	control_screen.appendChild(home_btn);
	home_btn.setPos(80, 220);

	var back_btn = createImageInput(BackBtnNormalDescription, BackBtnPressDescription, null, 73, 73);
	back_btn.up = function(){switch_back_screen();};
	control_screen.appendChild(back_btn);
	back_btn.setPos(180, 220);
	
	var cnt_climat_btn = createImageInput(ControlClimatBtnNormalDescription, ControlClimatBtnPressDescription, null, 220, 100);
	cnt_climat_btn.up = function(){switch_screen("climat_screen");};
	control_screen.appendChild(cnt_climat_btn);
	cnt_climat_btn.setPos(365, 220);

	var cnt_weather_btn = createImageInput(ControlWeatherBtnNormalDescription, ControlWeatherBtnPressDescription, null, 220, 100);
	cnt_weather_btn.up = function(){switch_screen("weather_screen");};
	control_screen.appendChild(cnt_weather_btn);
	cnt_weather_btn.setPos(617, 220);
	
	var cnt_cinema_btn = createImageInput(ControlCinemaBtnNormalDescription, ControlCinemaBtnPressDescription, null, 220, 100);
	control_screen.appendChild(cnt_cinema_btn);
	cnt_cinema_btn.setPos(870, 220);
	
	var cnt_alarm_btn = createImageInput(ControlAlarmBtnNormalDescription, ControlAlarmBtnPressDescription, null, 220, 100);
	cnt_alarm_btn.up = function(){switch_screen("alarm_screen");};
	control_screen.appendChild(cnt_alarm_btn);
	cnt_alarm_btn.setPos(365, 360);

	var cnt_curtains_btn = createImageInput(ControlCurtainsBtnNormalDescription, ControlCurtainsBtnPressDescription, null, 220, 100);
	cnt_curtains_btn.up = function(){switch_screen("curtains_screen");};
	control_screen.appendChild(cnt_curtains_btn);
	cnt_curtains_btn.setPos(617, 360);
	
	var cnt_video_btn = createImageInput(ControlVideoBtnNormalDescription, ControlVideoBtnPressDescription, null, 220, 100);
	control_screen.appendChild(cnt_video_btn);
	cnt_video_btn.setPos(870, 360);
	
	var cnt_water_btn = createImageInput(ControlWaterBtnNormalDescription, ControlWaterBtnPressDescription, null, 220, 100);
	cnt_water_btn.up = function(){switch_screen("water_screen");};
	control_screen.appendChild(cnt_water_btn);
	cnt_water_btn.setPos(365, 500);
	
	var cnt_electro_btn = createImageInput(ControlElectroBtnNormalDescription, ControlElectroBtnPressDescription, null, 220, 100);
	cnt_electro_btn.up = function(){switch_screen("electro_screen");};
	control_screen.appendChild(cnt_electro_btn);
	cnt_electro_btn.setPos(617, 500);
	
	var cnt_light_btn = createImageInput(ControlLightBtnNormalDescription, ControlLightBtnPressDescription, null, 220, 100);
	cnt_light_btn.up = function(){switch_screen("light_screen");};
	control_screen.appendChild(cnt_light_btn);
	cnt_light_btn.setPos(870, 500);
};

function switch_screen(new_screen_name)
{
	var new_screen = document.getElementById(new_screen_name);
	//alert(new_screen);
	
	if(new_screen == main_struct.history_screen[main_struct.history_screen.length - 1])return;
	
	main_struct.history_screen[main_struct.history_screen.length - 1].setAttribute("display", "none");
	
	new_screen.setAttribute("display", "block");
	
	main_struct.history_screen[main_struct.history_screen.length] = new_screen;
};

function switch_home_screen()
{
	main_struct.history_screen[main_struct.history_screen.length - 1].setAttribute("display", "none");

	while(main_struct.history_screen.length > 1)
		main_struct.history_screen.pop();
	
	main_struct.history_screen[0].setAttribute("display", "block");	
};

function switch_back_screen()
{
	main_struct.history_screen[main_struct.history_screen.length - 1].setAttribute("display", "none");
	main_struct.history_screen.pop();
	
	main_struct.history_screen[main_struct.history_screen.length - 1].setAttribute("display", "block");	
};

function show_help_screen(text)
{
try{
	var help_screen = document.getElementById("help_screen");

	
	if(text)
	{
		var text_container = help_screen.getElementsByTagName("g")[0];
		var i;

		while(text_container.childNodes.length > 0)
			text_container.removeChild(text_container.childNodes[0]);
			
		var strXPos = 230;
		var strYPos = 200;
		var strStep = 30;
		
		for(i = 0; i < text.length; i++)
		{
			var new_string = createMyElement(
			{
				"name": "text",
				"attr":
				{
					"font-family": "Arial",
					"font-size": "20",
					"fill": "white",
					"x": strXPos,
					"y": strYPos + strStep*i
				},
				"child":
				[
					text[i]
				]
			});
			text_container.appendChild(new_string);
		}
	}
	
	help_screen.setAttribute("display", "block");
}
catch(err)	
{
	alert(err);
}

};

function hide_help_screen()
{
	var help_screen = document.getElementById("help_screen");
	help_screen.setAttribute("display", "none");
};

function show_info_screen()
{
	var info_screen = document.getElementById("info_screen");
	info_screen.setAttribute("display", "block");
	
	main_struct.notice.showNew();
};

function hide_info_screen()
{
	var info_screen = document.getElementById("info_screen");
	info_screen.setAttribute("display", "none");	
};

function clockProcess()
{
	try{
	var dt = new Date();
	
	var hour = dt.getHours();
	var minute = dt.getMinutes();
	
	var date = dt.getDate();
	var month = dt.getMonth();
	
	var dow = dt.getDay();
	
	switch(month)
	{
		case 0:
			month = "января";
			break;
		case 1:
			month = "февраля";
			break;
		case 2:
			month = "марта";
			break;
		case 3:
			month = "апреля";
			break;
		case 4:
			month = "мая";
			break;
		case 5:
			month = "июня";
			break;
		case 6:
			month = "июля";
			break;
		case 7:
			month = "августа";
			break;
		case 8:
			month = "сентября";
			break;
		case 9:
			month = "октября";
			break;
		case 10:
			month = "ноября";
			break;
		case 11:
			month = "декабря";
			break;
	}
	
	switch(dow)
	{
		case 0:
			dow = "воскресенье";
			break;
		case 1:
			dow = "понедельник";
			break;
		case 2:
			dow = "вторник";
			break;
		case 3:
			dow = "среда";
			break;
		case 4:
			dow = "четверг";
			break;
		case 5:
			dow = "пятница";
			break;
		case 6:
			dow = "суббота";
			break;
	}
	
	var str = date + " " + month + ", " + dow;
	//alert(str + " " + hour + ":" + minute);
	
	if(str != main_struct.old_clock)
	{
		var clock_text = document.getElementById("clock_image").getElementsByTagName("g")[0];
		while(clock_text.childNodes.length > 0)
			clock_text.removeChild(clock_text.childNodes[0]);
		
		var letter_width = 6.8;
		var clock_content = createMyElement(
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "14",
				"font-weight": "500",
				"fill": "white",
				"y": "40",
				"x": -(str.length*letter_width)/2
			},
			"child":
			[
				str
			]
		});
		clock_text.appendChild(clock_content);
		main_struct.old_clock = str;
	}
	
	minute = "0" + minute;
	hour = "0" + hour;
	
	var hm = document.getElementById("clock_image").getElementsByTagName("image");
	hm[1].setAttributeNS("http://www.w3.org/1999/xlink", "href", "img/clock/" + hour.substr(-2, 1) + ".png");
	hm[2].setAttributeNS("http://www.w3.org/1999/xlink", "href", "img/clock/" + hour.substr(-1, 1) + ".png");
	
	hm[3].setAttributeNS("http://www.w3.org/1999/xlink", "href", "img/clock/" + minute.substr(-2, 1) + ".png");
	hm[4].setAttributeNS("http://www.w3.org/1999/xlink", "href", "img/clock/" + minute.substr(-1, 1) + ".png");
	}
	catch(err)
	{
		alert(err);
	}
};


function info_screen_init()
{
	main_struct.new_info_display = createDisplayControl(1, null);
	//main_struct.new_info_display.prefix = "%h:%m ";
	var info_screen = document.getElementById("info_screen").getElementsByTagName("g")[0];
		
	
	info_screen.appendChild(main_struct.new_info_display);
	main_struct.new_info_display.moveX(220);
	main_struct.new_info_display.moveY(175);
	main_struct.new_info_display.moveW(540);
	main_struct.new_info_display.moveH(420);
	main_struct.new_info_display.charCount = 40;
	main_struct.new_info_display.color = "white";
	main_struct.new_info_display.clear();
		
	var toogle_state  = createCombOutput(
	{
		"name": "text",
		"attr":
		{
			"x": "-20",
			"y": "5",
			"font-family": "Arial",
			"font-size": "25",
			"fill": "white"
		},
		"child":
		[
			"все"
		]
	},
	{
		"name": "text",
		"attr":
		{
			"x": "-35",
			"y": "5",
			"font-family": "Arial",
			"font-size": "25",
			"fill": "white"
		},
		"child":
		[
			"новые"
		]
	},
	null);
	
	main_struct.notice = new Notice(main_struct.new_info_display, 
									main_struct.db,
									function()
									{
										toogle_state.off();
										main_struct.new_info_display.scrollMove(100);
									},
									function()
									{
										toogle_state.on();
										main_struct.new_info_display.scrollMove(100);
									});
	main_struct.notice.prefix = "%h:%m ";
	appendDevArray(main_struct.displayArray, 20, main_struct.notice);
	
	var close_btn = createImageInput(
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-18",
					"y": "-18",
					"width": "35",
					"height": "35",
					"href": ["http://www.w3.org/1999/xlink", "img_cnt/close.png"]
				}
			}
		]
	},
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-18",
					"y": "-18",
					"width": "35",
					"height": "35",
					"href": ["http://www.w3.org/1999/xlink", "img_cnt/close.png"]
				}
			}
		]
	},
	184,
	35, 35);
	info_screen.parentNode.appendChild(close_btn);
	close_btn.setPos(780, 200);
	close_btn.up = function()
	{
		hide_info_screen();
		main_struct.notice.mark();
	};
	
	var toogle_btn = createImageInput(
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-71",
					"y": "-18",
					"width": "143",
					"height": "36",
					"href": ["http://www.w3.org/1999/xlink", "img/alarm/btn_pas.png"]
				}
			}
		]
	},
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-71",
					"y": "-18",
					"width": "143",
					"height": "36",
					"href": ["http://www.w3.org/1999/xlink", "img/alarm/btn_pas.png"]
				}
			}
		]
	},
	null,
	140, 35);
	info_screen.parentNode.appendChild(toogle_btn);
	toogle_btn.setPos(726, 615);
	
	toogle_btn.ind.appendChild(toogle_state);
	
	toogle_btn.up = function()
	{
		if(main_struct.notice.all)
		{
			main_struct.notice.showNew();
		}
		else
		{
			main_struct.notice.showAll();
		}
	};
	
/*	main_struct.new_info_display.setText(1, "{\"text\": \"http://172.16.128.68/index.php\
\
//==== Люди из ОИР, присутствующие на объекте в 14:16 25го марта 2009 года =========\
\
select * from persons where descr in (\
select ev_io.per as pers\
from\
(select ev.person as per, ev.glob_obj as go, ev.comp_event as ce\
from events ev inner join (select person as per, max(event_time) as dt\
                           from (select person, event_time \
                                 from events \
                                 where datetime < '2009-03-25 14:16:00') e\
                           group by person) e2\
on ev.person = e2.per and ev.datetime = e2.dt) ev_io\
inner join ios io\
on ev_io.go = io.glob_obj and ev_io.ce = io.comp_event\
where io.type in (0, 2)) and depart = 11\
\
//==================================================================================\
\
//======== Интервалы присутствия по людям с 18 февраля по 25 марта включительно=====\
\
select inp.pers as person, inp.dt as enter, min(outp.dt) as ex  \
from \
(select e.datetime as dt, e.person as pers \
from \
(select datetime, person, id, glob_obj, comp_event from events \
where datetime > '2009-02-18 00:00:00' \
and datetime < '2009-03-26 00:00:00') e \
inner join ios i \
on e.glob_obj = i.glob_obj and e.comp_event = i.comp_event \
where i.type = 0) inp \
inner join \
(select e.datetime as dt, e.person as pers \
from \
(select datetime, person, id, glob_obj, comp_event from events \
where datetime > '2009-02-18 00:00:00' \
and datetime < '2009-03-26 00:00:00') e \
inner join ios i \
on e.glob_obj = i.glob_obj and e.comp_event = i.comp_event \
where i.type = 1) outp \
on inp.pers = outp.pers and inp.dt < outp.dt and \
(inp.dt + interval 2 day) > outp.dt\
group by inp.pers, inp.dt\
\", \"color\": \"red\", \"icon\": \"icon1\"}");*/
//	main_struct.notice.setText(20, "{\"text\": \"Привет!!!\", \"icon\": \"icon2\"}");
//	main_struct.notice.setText(20, "Привет, народ!");
	
	//main_struct.new_info_display.scrollMove(0);
	
	main_struct.info_scroll = new scrollBar(ScrollDescr, null);
	main_struct.info_scroll.free_callback = function(y)
	{
		main_struct.new_info_display.scrollMove(y);
	};
	info_screen.parentNode.appendChild(main_struct.info_scroll.base_el);
	main_struct.info_scroll.setPos(780, 400);
	main_struct.info_scroll.setScrollPos(100);
};

function test_notice()
{
//	main_struct.notice.setText(20, "{\"text\": \"Уведомление №1\", \"icon\": \"icon1\", \"color\": \"green\"}");
//	main_struct.notice.setText(20, "{\"text\": \"Уведомление №2\", \"icon\": \"icon1\", \"color\": \"lime\"}");
};

function test_notice2()
{
//	main_struct.notice.setText(20, "{\"text\": \"Тревога!!!\", \"icon\": \"icon2\", \"color\": \"red\"}");

	try{
	common.hide();
	}
	catch(err)
	{
		alert(err);
	}
};


