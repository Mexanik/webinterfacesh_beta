
//var elctroReg;

function build_electro_screen(config)
{
	var wp = document.getElementById("electro_screen_wp2");
	wp.current_room = null;
	
	var ind;
	
	var sens1 = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "20",
		"fill":	"white"
	},
	config.sens[0].format,
	config.sens[0].addr);
	sens1.regExp = config.sens[0].re;
	wp.appendChild(sens1);
	sens1.set("0");
	sens1.setPos(600, 250);
	appendDevArray(main_struct.analogOutArray, config.sens[0].addr, sens1);
	
	var sens2 = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "20",
		"fill":	"white"
	},
	config.sens[1].format,
	config.sens[1].addr);
	sens2.regExp = config.sens[1].re;
	wp.appendChild(sens2);
	sens2.set("0");
	sens2.setPos(600, 320);
	appendDevArray(main_struct.analogOutArray, config.sens[1].addr, sens2);
	
	var btn1 = createImageInput(ElectroSwitchBtnNormalDescription, ElectroSwitchBtnPressDescription, config.btn[0].btn_addr, 148, 46);
	btn1.setAttribute("transform", "translate(560 382)");
	wp.appendChild(btn1);
	ind = createCombOutput(
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-64",
					"y": "-14",
					"width": "28",
					"height": "28",
					"href": ["http://www.w3.org/1999/xlink", "img/electro/on.png"]
				}
			},
			{
				"name": "text",
				"attr":
				{
					"font-family": "Arial",
					"font-size": "16",
					"font-weight": "bold",
					"fill": "white",
					"x": "-30",
					"y": "5"
				},
				"child":
				[
					"Выключить"
				]
			}
		]
	},
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-64",
					"y": "-14",
					"width": "28",
					"height": "28",
					"href": ["http://www.w3.org/1999/xlink", "img/electro/off.png"]
				}
			},
			{
				"name": "text",
				"attr":
				{
					"font-family": "Arial",
					"font-size": "16",
					"font-weight": "bold",
					"fill": "white",
					"x": "-30",
					"y": "5"
				},
				"child":
				[
					"Включить"
				]
			}
		]
	},
	config.btn[0].ind_addr);
	ind.off();
	appendDevArray(main_struct.outputArray, config.btn[0].ind_addr, ind);
	btn1.ind.appendChild(ind);
	
	var btn2 = createImageInput(ElectroSwitchBtnNormalDescription, ElectroSwitchBtnPressDescription, config.btn[1].btn_addr, 148, 46);
	btn2.setAttribute("transform", "translate(560 450)");
	wp.appendChild(btn2);
	ind = createCombOutput(
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-64",
					"y": "-14",
					"width": "28",
					"height": "28",
					"href": ["http://www.w3.org/1999/xlink", "img/electro/on.png"]
				}
			},
			{
				"name": "text",
				"attr":
				{
					"font-family": "Arial",
					"font-size": "16",
					"font-weight": "bold",
					"fill": "white",
					"x": "-30",
					"y": "5"
				},
				"child":
				[
					"Выключить"
				]
			}
		]
	},
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-64",
					"y": "-14",
					"width": "28",
					"height": "28",
					"href": ["http://www.w3.org/1999/xlink", "img/electro/off.png"]
				}
			},
			{
				"name": "text",
				"attr":
				{
					"font-family": "Arial",
					"font-size": "16",
					"font-weight": "bold",
					"fill": "white",
					"x": "-30",
					"y": "5"
				},
				"child":
				[
					"Включить"
				]
			}
		]
	},
	config.btn[1].ind_addr);
	ind.off();
	appendDevArray(main_struct.outputArray, config.btn[1].ind_addr, ind);
	btn2.ind.appendChild(ind);
	
	var btn3 = createImageInput(ElectroSwitchBtnNormalDescription, ElectroSwitchBtnPressDescription, config.btn[2].btn_addr, 148, 46);
	btn3.setAttribute("transform", "translate(560 518)");
	wp.appendChild(btn3);
	ind = createCombOutput(
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-64",
					"y": "-14",
					"width": "28",
					"height": "28",
					"href": ["http://www.w3.org/1999/xlink", "img/electro/on.png"]
				}
			},
			{
				"name": "text",
				"attr":
				{
					"font-family": "Arial",
					"font-size": "16",
					"font-weight": "bold",
					"fill": "white",
					"x": "-30",
					"y": "5"
				},
				"child":
				[
					"Выключить"
				]
			}
		]
	},
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-64",
					"y": "-14",
					"width": "28",
					"height": "28",
					"href": ["http://www.w3.org/1999/xlink", "img/electro/off.png"]
				}
			},
			{
				"name": "text",
				"attr":
				{
					"font-family": "Arial",
					"font-size": "16",
					"font-weight": "bold",
					"fill": "white",
					"x": "-30",
					"y": "5"
				},
				"child":
				[
					"Включить"
				]
			}
		]
	},
	config.btn[2].ind_addr);
	ind.off();
	appendDevArray(main_struct.outputArray, config.btn[2].ind_addr, ind);
	btn3.ind.appendChild(ind);
		
	var chart = new LineChart();
	
	chart.setPos(600, 384);
	chart.setSize(700, 300);
		
	chart.hide();
	
	var elctroReg = new ElectroRegistrater(main_struct.db, chart);
	appendDevArray(main_struct.analogOutArray, config.sens[1].addr, elctroReg);
	
	var stat_btn = createImageInput(
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-74",
					"y": "-23",
					"width": "148",
					"height": "46",
					"href": ["http://www.w3.org/1999/xlink", "img/electro/btn.png"]
				}
			},
			{
				"name": "text",
				"attr":
				{
					"fill": "white",
					"font-family": "Arial",
					"font-size": "18",
					"x": -45,
					"y": 5
				},
				"child":
				[
					"статистика"
				]
			}
		]
	},
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-74",
					"y": "-23",
					"width": "148",
					"height": "46",
					"href": ["http://www.w3.org/1999/xlink", "img/electro/btn.png"]
				}
			},
			{
				"name": "text",
				"attr":
				{
					"fill": "white",
					"font-family": "Arial",
					"font-size": "18",
					"x": -45,
					"y": 5
				},
				"child":
				[
					"статистика"
				]
			}
		]
	},
	null,
	148, 46);
	
	stat_btn.up = function()
	{
		elctroReg.show();
	};
	
	stat_btn.setPos(850, 315);
	wp.appendChild(stat_btn);	
	
	wp.appendChild(chart.base_el);
};


function electro_test_show()
{
	elctroReg.setText(0, ((new Date()).getTime() >>> 10) % 10);
}



