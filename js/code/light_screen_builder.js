
var RoomMenuItem =
{
	"name": "g",
	"child":
	[
		{
			"name": "image",
			"attr":
			{
				"x": "-70",
				"y": "-17",
				"width": "140",
				"height": "33",
				"href": ["http://www.w3.org/1999/xlink", "img/light/btn_place.png"],
				"display": "none",
				"class": "select"
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-44",
				"y": "6",
				"class": "text"
			},
			"child":
			[
				"some text"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-70",
				"y": "-17",
				"width": "140",
				"height": "33",
				"fill": "red",
				"fill-opacity": "0",
				"class": "rect"
			}
		}/*,
		{
			"name": "g",
			"attr":
			{
				"display": "none",
				"class": "container"
			},
			"child":
			[
				{
					"name": "rect",
					"attr":
					{
						"x": "0",
						"y": "0",
						"width": "200",
						"height": "200",
						"fill": "red"
					}
				}
			]
		}*/
	]
};

var SimpleLamp = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-245",
				"y": "7"
			},
			"child":
			[
				"Simple lamp"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "40",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/light/horiz_line.png"]
			}
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var SimpleLampR = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-245",
				"y": "7"
			},
			"child":
			[
				"Simple R lamp"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "284",
				"height": "80",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};

var DimLamp = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-245",
				"y": "7"
			},
			"child":
			[
				"Dim lamp"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "40",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/light/horiz_line.png"]
			}
		},
		{
			"name": "image",
			"attr":
			{
				"x": "0",
				"y": "-14",
				"width": "113",
				"height": "28",
				"href": ["http://www.w3.org/1999/xlink", "img/light/-bg+.png"]
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "red",
				"fill-opacity": "0"
			}
		}
	]
};

function build_light_screen(light_config)
{
	var wp = document.getElementById("light_screen_wp2");
	wp.current_room = null;
	
	var room_index;
	var lamp_index;
	var new_item;

	var item_step = 42;
	var base = 384 - (light_config.length * item_step)/2;
	var first_selected = null;
	for(room_index = 0; room_index < light_config.length; room_index++)
	{
		RoomMenuItem.child[1].child[0] = light_config[room_index].name;
		new_item = createMyElement(RoomMenuItem)
		new_item.setAttribute("transform", "translate(900 " + ( base + room_index*item_step ) + ")");
		new_item.select = getElementByClass(new_item, "select");
		new_item.text = getElementByClass(new_item, "text");
		new_item.container = createMyElement(
		{
			"name": "g", 
			"attr": 
			{
				"display": "none"
			}
		});
		//alert(new_item.container);
		new_item.rect = getElementByClass(new_item, "rect");
		new_item.rect.onclick = function()
		{
			var el = event.target.parentNode;
			if(wp.current_room != null)
			{
				if(wp.current_room == el)return;
				wp.current_room.text.setAttribute("fill", "white");
				wp.current_room.select.setAttribute("display", "none");
				wp.current_room.container.setAttribute("display", "none");
			}
			
			el.text.setAttribute("fill", "black");
			el.select.setAttribute("display", "block");
			el.container.setAttribute("display", "block");
			wp.current_room = el;
		};
		
		var lamp_posY = 200;
		var lamp_posX = 524;
		var lamp_step = 80;
		var new_lamp;
		for(lamp_index = 0; lamp_index < light_config[room_index].objects.length; lamp_index++)
		{
			var lamp_config = light_config[room_index].objects[lamp_index];
			
			switch(lamp_config.type)
			{
				case "simple":
					SimpleLamp.child[0].child[0] = lamp_config.name;
					if(lamp_config.unline == true)
					{
						SimpleLamp.child[1].attr.display = "none";
					}
					else
					{
						SimpleLamp.child[1].attr.display = "block";
					}
					new_lamp = createMyElement(SimpleLamp);
					//new_lamp.container = getElementByClass(new_lamp, "container");
					new_lamp.setAttribute("transform", 
						"translate(" + lamp_posX + " " + lamp_posY + ")");
					new_lamp.btn = createImageInput(SimpleLightBtnNormalDescription, SimpleLightBtnPressDescription, lamp_config.btn_addr, 74, 74);
					var ind = createImageOutput(
					{
						"w": 44,
						"h": 58,
						"img": "img/light/lamp_on.png"
					},
					{	
						"w": 35,
						"h": 58,
						"img": "img/light/lamp_off.png"
					},
					lamp_config.ind_addr);
					main_struct.outputArray[lamp_config.ind_addr] = [ind];
					new_lamp.btn.ind.appendChild(ind);
					ind.off();
					new_lamp.btn.setAttribute("transform", "translate(-50 0)");
					new_lamp.appendChild(new_lamp.btn);
					
					new_item.container.appendChild(new_lamp);
					lamp_posY += lamp_step;
					break;
					
				case "simple_r":
					lamp_posY -= lamp_step;
					SimpleLampR.child[0].child[0] = lamp_config.name;
					new_lamp = createMyElement(SimpleLampR);
					new_lamp.setAttribute("transform", 
						"translate(" + (lamp_posX + 285) + " " + lamp_posY + ")");
					new_lamp.btn = createImageInput(SimpleLightBtnNormalDescription, SimpleLightBtnPressDescription, lamp_config.btn_addr, 74, 74);
					var ind = createImageOutput(
					{
						"w": 44,
						"h": 58,
						"img": "img/light/lamp_on.png"
					},
					{	
						"w": 35,
						"h": 58,
						"img": "img/light/lamp_off.png"
					},
					lamp_config.ind_addr);
					new_lamp.btn.ind.appendChild(ind);
					main_struct.outputArray[lamp_config.ind_addr] = [ind];
					ind.off();
					new_lamp.btn.setAttribute("transform", "translate(-50 0)");
					new_lamp.appendChild(new_lamp.btn);
					
					new_item.container.appendChild(new_lamp);
					lamp_posY += lamp_step;					
					break;
					
				case "fan":
					SimpleLamp.child[0].child[0] = lamp_config.name;
					if(lamp_config.unline == true)
					{
						SimpleLamp.child[1].attr.display = "none";
					}
					else
					{
						SimpleLamp.child[1].attr.display = "block";
					}
					new_lamp = createMyElement(SimpleLamp);
					//new_lamp.container = getElementByClass(new_lamp, "container");
					new_lamp.setAttribute("transform", 
						"translate(" + lamp_posX + " " + lamp_posY + ")");
					new_lamp.btn = createImageInput(SimpleLightBtnNormalDescription, SimpleLightBtnPressDescription, lamp_config.btn_addr, 74, 74);
					var ind = createImageOutput(
					{
						"w": 35,
						"h": 35,
						"img": "img/light/vent_on.png"
					},
					{	
						"w": 35,
						"h": 35,
						"img": "img/light/vent_off.png"
					},
					lamp_config.ind_addr);
					main_struct.outputArray[lamp_config.ind_addr] = [ind];
					new_lamp.btn.ind.appendChild(ind);
					ind.off();
					new_lamp.btn.setAttribute("transform", "translate(-50 0)");
					new_lamp.appendChild(new_lamp.btn);
					
					new_item.container.appendChild(new_lamp);
					lamp_posY += lamp_step;
					break;
					
				case "fan_r":
					lamp_posY -= lamp_step;
					SimpleLampR.child[0].child[0] = lamp_config.name;
					new_lamp = createMyElement(SimpleLampR);
					new_lamp.setAttribute("transform", 
						"translate(" + (lamp_posX + 285) + " " + lamp_posY + ")");
					new_lamp.btn = createImageInput(SimpleLightBtnNormalDescription, SimpleLightBtnPressDescription, lamp_config.btn_addr, 74, 74);
					var ind = createImageOutput(
					{
						"w": 35,
						"h": 35,
						"img": "img/light/vent_on.png"
					},
					{	
						"w": 35,
						"h": 35,
						"img": "img/light/vent_off.png"
					},
					lamp_config.ind_addr);
					new_lamp.btn.ind.appendChild(ind);
					main_struct.outputArray[lamp_config.ind_addr] = [ind];
					ind.off();
					new_lamp.btn.setAttribute("transform", "translate(-50 0)");
					new_lamp.appendChild(new_lamp.btn);
					
					new_item.container.appendChild(new_lamp);
					lamp_posY += lamp_step;					
					break;
					
				case "dim":
					DimLamp.child[0].child[0] = lamp_config.name;
					if(lamp_config.unline == true)
					{
						DimLamp.child[1].attr.display = "none";
					}
					else
					{
						DimLamp.child[1].attr.display = "block";
					}
					new_lamp = createMyElement(DimLamp);
					new_lamp.setAttribute("transform", 
						"translate(" + lamp_posX + " " + lamp_posY + ")");
					new_lamp.btn_plus = createImageInput(PlusLightBtnNormalDescription, PlusLightBtnPressDescription, lamp_config.plus_addr, 35, 35);
					new_lamp.btn_plus.setAttribute("transform", "translate(113 2)");
					new_lamp.appendChild(new_lamp.btn_plus);
					new_lamp.btn_minus = createImageInput(MinusLightBtnNormalDescription, MinusLightBtnPressDescription, lamp_config.minus_addr, 35, 35);
					new_lamp.btn_minus.setAttribute("transform", "translate(0 2)");
					new_lamp.appendChild(new_lamp.btn_minus);
					new_lamp.btn_max = createImageInput(MaxLightBtnNormalDescription, MaxLightBtnPressDescription, lamp_config.on_addr, 86, 31);
					new_lamp.btn_max.setAttribute("transform", "translate(190 2)");
					new_lamp.appendChild(new_lamp.btn_max);
					new_lamp.btn_min = createImageInput(MinLightBtnNormalDescription, MinLightBtnPressDescription, lamp_config.off_addr, 86, 31);
					new_lamp.btn_min.setAttribute("transform", "translate(-77 2)");
					new_lamp.appendChild(new_lamp.btn_min);
					
					if(lamp_config.dim_addr != undefined)
					{
						new_lamp.dim_out = createTextOutput(
						{
							"font-family": "Courier",
							"font-size": "20",
							"font-weight": "800",
							"fill": "white",
							"x": "0",
							"y": "0"
						},
						"%D%", lamp_config.dim_addr);
						new_lamp.dim_out.align_center = 13;
						new_lamp.appendChild(new_lamp.dim_out);
						new_lamp.dim_out.setPos(60, 8);
						new_lamp.dim_out.set(0);
						appendDevArray(main_struct.analogOutArray, lamp_config.dim_addr, new_lamp.dim_out);
					}
					
					
					new_item.container.appendChild(new_lamp);
					lamp_posY += lamp_step;
					break;
			}
						
		}
		
		var item_index;
		var item_posX = 133;
		var item_posY = 420;
		var item_step = 40;
		for(item_index = 0; item_index < light_config[room_index].scene.length; item_index++)
		{
			var scene_config = light_config[room_index].scene[item_index];
			//alert(scene_config.btn_addr);
			var scene_btn = createImageInput(
			{
				"name": "g"
			},
			{
				"name": "g"
			},
			scene_config.btn_addr, 190, 36);
			scene_btn.setPos(item_posX, item_posY + item_index*item_step);
			new_item.container.appendChild(scene_btn);
			
			var scene_ind = createCombOutput(
			{
				"name": "g",
				"child":
				[
					{
						"name": "image",
						"attr":
						{
							"x": "-95",
							"y": "-18",
							"width": "190",
							"height": "36",
							"href": ["http://www.w3.org/1999/xlink", "img/light/btn_scene.png"]
						}
					},
					{
						"name": "text",
						"attr":
						{
							"font-family": "Arial",
							"font-size": "20",
							"fill": "black",
							"x": "-60",
							"y": "7"
						},
						"child":
						[
							scene_config.name
						]
					}
				]
			},
						{
				"name": "g",
				"child":
				[
					{
						"name": "text",
						"attr":
						{
							"font-family": "Arial",
							"font-size": "20",
							"fill": "white",
							"x": "-60",
							"y": "7"
						},
						"child":
						[
							scene_config.name
						]
					}
				]
			}, scene_config.ind_addr);
			scene_ind.off();
			appendDevArray(main_struct.outputArray, scene_config.ind_addr, scene_ind);
			scene_btn.ind.appendChild(scene_ind);
		}
		
		wp.appendChild(new_item);
		wp.appendChild(new_item.container);
		if(room_index == 0)
		{
			first_selected = new_item;
		}
	}
	
	if(first_selected != null)
	{
		first_selected.text.setAttribute("fill", "black");
		first_selected.select.setAttribute("display", "block");
		first_selected.container.setAttribute("display", "block");
		wp.current_room = first_selected;
	}
		
};



