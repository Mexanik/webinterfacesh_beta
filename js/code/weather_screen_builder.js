
function build_weather_screen(config)
{
	var wp = document.getElementById("weather_screen_wp2");
	
	var icon = createMultiImageOutput(
	[
		{
			"w": "100",
			"h": "100",
			//"img": "img/weather/yasno.png",
			"img": "img/weather/sun.png",
			"value": "fair"
		},
		{
			"w": "100",
			"h": "100",
			//"img": "img/weather/dozd.png",
			"img": "img/weather/rain.png",
			"value": "rain"
		},
		{
			"w": "100",
			"h": "100",
			//"img": "img/weather/sneg.png",
			"img": "img/weather/snow.png",
			"value": "snow"
		},
		{
			"w": "100",
			"h": "100",
			//"img": "img/weather/oblachno.png",
			"img": "img/weather/cloudly.png",
			"value": "cloud"
		}
	],
	config.ind_addr);
	appendDevArray(main_struct.displayArray, config.ind_addr, icon);
	icon.setAttribute("transform", "translate(135 400)");
	icon.set("fair");
	wp.appendChild(icon);
	
	var wind_dir = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "30",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%s",
	config.wind_dir_addr);
	main_struct.displayArray[config.wind_dir_addr] = [wind_dir];
	wind_dir.setAttribute("transform", "translate(550 395)");
	wind_dir.set("");
	wp.appendChild(wind_dir);

	var term = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "30",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%d C" + String.fromCharCode(176),
	config.outside_term_addr);
	appendDevArray(main_struct.analogOutArray, config.outside_term_addr, term);
	term.setAttribute("transform", "translate(550 247)");
	term.set("22.2");
	wp.appendChild(term);

	var damp = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "30",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%D%",
	config.outside_damp_addr);
	appendDevArray(main_struct.analogOutArray, config.outside_damp_addr, damp);
	damp.setAttribute("transform", "translate(550 297)");
	damp.set("32");
	wp.appendChild(damp);

	var pressure = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "30",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%d гПа",
	config.pressure_addr);
	main_struct.analogOutArray[config.pressure_addr] = [pressure];
	pressure.setAttribute("transform", "translate(550 345)");
	pressure.set("1000");
	wp.appendChild(pressure);
	
	var wind_speed = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "30",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%D м/с",
	config.wind_speed_addr);
	main_struct.analogOutArray[config.wind_speed_addr] = [wind_speed];
	wind_speed.setAttribute("transform", "translate(550 445)");
	wind_speed.set("5");
	wp.appendChild(wind_speed);
	
/*	var fallout_count = createTextOutput(
	{
		"font-family": "Arial",
		"font-size": "30",
		"fill": "white",
		"x": "0",
		"y": "10"
	},
	"%D мм.",
	config.fallout_count_addr);
	main_struct.analogOutArray[config.fallout_count_addr] = [fallout_count];
	fallout_count.setAttribute("transform", "translate(550 497)");
	fallout_count.set("320");
	wp.appendChild(fallout_count);*/
};




