
var AlarmPageMenuItem =
{
	"name": "g",
	"child":
	[
		{
			"name": "image",
			"attr":
			{
				"x": "-70",
				"y": "-17",
				"width": "140",
				"height": "33",
				"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_list.png"],
				"display": "none",
				"class": "select"
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-weight": "bold",
				"font-size": "13",
				"fill": "white",
				"x": "-62",
				"y": "3",
				"class": "text"
			},
			"child":
			[
				"some text"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-70",
				"y": "-17",
				"width": "140",
				"height": "33",
				"fill": "red",
				"fill-opacity": "0",
				"class": "rect"
			}
		}
	]
};

var SimpleAlarm = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-245",
				"y": "7"
			},
			"child":
			[
				"Simple alarm"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "30",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/line_h.png"]
			}
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var SimpleAlarmR = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-245",
				"y": "7"
			},
			"child":
			[
				"Simple R alarm"
			]
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "284",
				"height": "80",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};

var SmallAlarmL = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "15",
				"fill": "white",
				"x": "-250",
				"y": "7"
			},
			"child":
			[
				"Small alarm"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "15",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/line_h.png"]
			}
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var SmallAlarmM = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "15",
				"fill": "white",
				"x": "-60",
				"y": "7"
			},
			"child":
			[
				"Small alarm"
			]
		},
/*		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "15",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/line_h.png"]
			}
		},*/
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var SmallAlarmR = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "15",
				"fill": "white",
				"x": "130",
				"y": "7"
			},
			"child":
			[
				"Small alarm"
			]
		},
/*		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "15",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/line_h.png"]
			}
		},*/
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var LongAlarm = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-245",
				"y": "7"
			},
			"child":
			[
				"Long alarm"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "30",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/alarm/line_h.png"]
			}
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};


function build_alarm_screen(config)
{
	var wp = document.getElementById("alarm_screen_wp2");
	wp.current_room = null;
	
	var room_index;
	var page_index;
	var new_item;

	var item_step = 42;
	var base = 384 - (config.length * item_step)/2;
	var first_selected = null;
	for(page_index = 0; page_index < config.length; page_index++)
	{
		AlarmPageMenuItem.child[1].child[0] = config[page_index].name;
		new_item = createMyElement(AlarmPageMenuItem)
		new_item.setAttribute("transform", "translate(900 " + ( base + page_index*item_step ) + ")");
		new_item.select = getElementByClass(new_item, "select");
		new_item.text = getElementByClass(new_item, "text");
		new_item.container = createMyElement(
		{
			"name": "g", 
			"attr": 
			{
				"display": "none"
			}
		});
		//alert(new_item.container);
		new_item.rect = getElementByClass(new_item, "rect");
		new_item.rect.onclick = function()
		{
			var el = event.target.parentNode;
			if(wp.current_room != null)
			{
				if(wp.current_room == el)return;
				wp.current_room.text.setAttribute("fill", "white");
				wp.current_room.select.setAttribute("display", "none");
				wp.current_room.container.setAttribute("display", "none");
			}
			
			el.text.setAttribute("fill", "black");
			el.select.setAttribute("display", "block");
			el.container.setAttribute("display", "block");
			wp.current_room = el;
		};

		
		var room_posY = 220;
		var room_posX = 524;
		var room_step = 60;
		var room_step2 = 30;
		var new_room;
		for(room_index = 0; room_index < config[page_index].objects.length; room_index++)
		{
			var room_config = config[page_index].objects[room_index];
			
			switch(room_config.type)
			{
				case "simple":
					SimpleAlarm.child[0].child[0] = room_config.name;
					if(room_config.unline == true)
					{
						SimpleAlarm.child[1].attr.display = "none";
					}
					else
					{
						SimpleAlarm.child[1].attr.display = "block";
					}
					new_room = createMyElement(SimpleAlarm);
					new_room.container = getElementByClass(new_room, "container");
					new_room.setAttribute("transform", 
						"translate(" + room_posX + " " + room_posY + ")");
					
					new_room.ind = createMultiImageOutput(
					[
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/blue.png"
						},
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/green.png"
						},
						{
							"w": 49,
							"h": 49,
							"img": "img/alarm/red.png"
						},
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/gray.png"
						}
					], room_config.addr);
					new_room.ind.set(0);
					new_room.ind.setAttribute("transform", "translate(100 0)");
					appendDevArray(main_struct.analogOutArray, room_config.addr, new_room.ind);
					new_room.container.appendChild(new_room.ind);
					
					new_item.container.appendChild(new_room);
					room_posY += room_step;
					break;
					
				case "small_l":
					SmallAlarmL.child[0].child[0] = room_config.name;
					if(room_config.unline == true)
					{
						SmallAlarmL.child[1].attr.display = "none";
					}
					else
					{
						SmallAlarmL.child[1].attr.display = "block";
					}
					new_room = createMyElement(SmallAlarmL);
					new_room.container = getElementByClass(new_room, "container");
					new_room.setAttribute("transform", 
						"translate(" + room_posX + " " + room_posY + ")");
					
					new_room.ind = createMultiImageOutput(
					[
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/blue_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/green_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/red_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/gray_small.png"
						}
					], room_config.addr);
					new_room.ind.set(0);
					new_room.ind.setPos(-265, 0);
					appendDevArray(main_struct.analogOutArray, room_config.addr, new_room.ind);
					new_room.container.appendChild(new_room.ind);
					
					new_item.container.appendChild(new_room);
					room_posY += room_step2;
					break;
					
				case "small_m":
					room_posY -= room_step2;
					SmallAlarmM.child[0].child[0] = room_config.name;
					/*if(room_config.unline == true)
					{
						SmallAlarmM.child[1].attr.display = "none";
					}
					else
					{
						SmallAlarmM.child[1].attr.display = "block";
					}*/
					new_room = createMyElement(SmallAlarmM);
					new_room.container = getElementByClass(new_room, "container");
					new_room.setAttribute("transform", 
						"translate(" + room_posX + " " + room_posY + ")");
					
					new_room.ind = createMultiImageOutput(
					[
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/blue_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/green_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/red_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/gray_small.png"
						}
					], room_config.addr);
					new_room.ind.set(0);
					new_room.ind.setPos(-75, 0);
					appendDevArray(main_struct.analogOutArray, room_config.addr, new_room.ind);
					new_room.container.appendChild(new_room.ind);
					
					new_item.container.appendChild(new_room);
					room_posY += room_step2;
					break;
					
				case "small_r":
					room_posY -= room_step2;
					SmallAlarmR.child[0].child[0] = room_config.name;
					/*if(room_config.unline == true)
					{
						SmallAlarmR.child[1].attr.display = "none";
					}
					else
					{
						SmallAlarmR.child[1].attr.display = "block";
					}*/
					new_room = createMyElement(SmallAlarmR);
					new_room.container = getElementByClass(new_room, "container");
					new_room.setAttribute("transform", 
						"translate(" + room_posX + " " + room_posY + ")");
					
					new_room.ind = createMultiImageOutput(
					[
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/blue_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/green_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/red_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/gray_small.png"
						}
					], room_config.addr);
					new_room.ind.set(0);
					new_room.ind.setPos(115, 0);
					appendDevArray(main_struct.analogOutArray, room_config.addr, new_room.ind);
					new_room.container.appendChild(new_room.ind);
					
					new_item.container.appendChild(new_room);
					room_posY += room_step2;
					break;
					
				case "simple_l":
					SimpleAlarm.child[0].child[0] = room_config.name;
					if(room_config.unline == true)
					{
						SimpleAlarm.child[1].attr.display = "none";
					}
					else
					{
						SimpleAlarm.child[1].attr.display = "block";
					}
					new_room = createMyElement(SimpleAlarm);
					new_room.container = getElementByClass(new_room, "container");
					new_room.setAttribute("transform", 
						"translate(" + room_posX + " " + room_posY + ")");
					
					new_room.ind = createMultiImageOutput(
					[
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/blue.png"
						},
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/green.png"
						},
						{
							"w": 49,
							"h": 49,
							"img": "img/alarm/red.png"
						},
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/gray.png"
						}
					], room_config.addr);
					new_room.ind.set(0);
					new_room.ind.setAttribute("transform", "translate(-60 0)");
					appendDevArray(main_struct.analogOutArray, room_config.addr, new_room.ind);
					new_room.container.appendChild(new_room.ind);
					
					new_item.container.appendChild(new_room);
					room_posY += room_step;
					break;
					
				case "simple_r":
					room_posY -= room_step;
					SimpleAlarmR.child[0].child[0] = room_config.name;
					new_room = createMyElement(SimpleAlarmR);
					new_room.container = getElementByClass(new_room, "container");
					new_room.setAttribute("transform", 
						"translate(" + (room_posX + 285) + " " + room_posY + ")");
					
					new_room.ind = createMultiImageOutput(
					[
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/blue.png"
						},
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/green.png"
						},
						{
							"w": 49,
							"h": 49,
							"img": "img/alarm/red.png"
						},
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/gray.png"
						}
					], room_config.addr);
					new_room.ind.set(0);
					new_room.ind.setAttribute("transform", "translate(-60 0)");
					appendDevArray(main_struct.analogOutArray, room_config.addr, new_room.ind);
					new_room.container.appendChild(new_room.ind);
					
					new_item.container.appendChild(new_room);
					room_posY += room_step;					
					break;
					
				case "long":
					LongAlarm.child[0].child[0] = room_config.name;
					if(room_config.unline == true)
					{
						LongAlarm.child[1].attr.display = "none";
					}
					else
					{
						LongAlarm.child[1].attr.display = "block";
					}
					new_room = createMyElement(LongAlarm);
					new_room.container = getElementByClass(new_room, "container");
					new_room.setAttribute("transform", 
						"translate(" + room_posX + " " + room_posY + ")");
					
					new_room.ind = createMultiImageOutput(
					[
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/blue.png"
						},
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/green.png"
						},
						{
							"w": 49,
							"h": 49,
							"img": "img/alarm/red.png"
						},
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/gray.png"
						}
					], room_config.addr);
					new_room.ind.set(0);
					new_room.ind.setAttribute("transform", "translate(0 0)");
					appendDevArray(main_struct.analogOutArray, room_config.addr, new_room.ind);
					new_room.container.appendChild(new_room.ind);
					
					new_room.btn = createImageInput(SetAlarmBtnNormalDescription, SetAlarmLightBtnPressDescription, room_config.reset_addr, 100, 43);
					new_room.btn.setAttribute("transform", "translate(160 0)");
					new_room.appendChild(new_room.btn);
					
					new_item.container.appendChild(new_room);
					room_posY += room_step;
					break;
			}
		}
		
		wp.appendChild(new_item);
		wp.appendChild(new_item.container);
		if(page_index == 0)
		{
			first_selected = new_item;
		}
	}
	
	if(first_selected != null)
	{
		first_selected.text.setAttribute("fill", "black");
		first_selected.select.setAttribute("display", "block");
		first_selected.container.setAttribute("display", "block");
		wp.current_room = first_selected;
	}	
};
