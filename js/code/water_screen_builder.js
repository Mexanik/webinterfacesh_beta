
var ValveWaterItem = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-245",
				"y": "7"
			},
			"child":
			[
				"ValveWaterItem"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "25",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/light/horiz_line.png"]
			}
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "green",
				"fill-opacity": "0"
			}
		}
	]
};

var ValveWaterItemR = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-245",
				"y": "7"
			},
			"child":
			[
				"ValveWaterItemR"
			]
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "284",
				"height": "80",
				"fill": "blue",
				"fill-opacity": "0"
			}
		}
	]
};

var TextOutWaterItem = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-245",
				"y": "7"
			},
			"child":
			[
				"TextOutWaterItem"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "25",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/light/horiz_line.png"]
			}
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "red",
				"fill-opacity": "0"
			}
		}
	]
};

function build_water_screen(config)
{
	var wp = document.getElementById("water_screen_wp2");
		
	var item_index;

	var item_posY = 250;
	var item_posX = 550;
	var item_step = 50;
	var new_item;
	for(item_index = 0; item_index < config.length; item_index++)
	{
		var item_config = config[item_index];
		
		switch(item_config.type)
		{
			case "valve":
				ValveWaterItem.child[0].child[0] = item_config.name;
				if(item_config.unline == true)
				{
					ValveWaterItem.child[1].attr.display = "none";
				}
				else
				{
					ValveWaterItem.child[1].attr.display = "block";
				}
				new_item = createMyElement(ValveWaterItem);
				new_item.container = getElementByClass(new_item, "container");
				new_item.setAttribute("transform", 
					"translate(" + item_posX + " " + item_posY + ")");
//				new_lamp.btn = createImageInput(SimpleLightBtnNormalDescription, SimpleLightBtnPressDescription, lamp_config.btn_addr, 74, 74);
				var ind = createImageOutput(
				{
					"w": 66,
					"h": 34,
					"img": "img/water/clapan_on.png"
				},
				{	
					"w": 66,
					"h": 34,
					"img": "img/water/clapan_off.png"
				},
				item_config.addr);
				appendDevArray(main_struct.outputArray, item_config.addr, ind);
				new_item.container.appendChild(ind);
				ind.off();
				ind.setAttribute("transform", "translate(-50 0)");
//				new_lamp.btn.setAttribute("transform", "translate(-50 0)");
//				new_lamp.appendChild(new_lamp.btn);
				
				wp.appendChild(new_item);
				item_posY += item_step;
				break;
				
			case "valve_r":
				item_posY -= item_step;
				ValveWaterItemR.child[0].child[0] = item_config.name;
				new_item = createMyElement(ValveWaterItemR);
				new_item.container = getElementByClass(new_item, "container");
				new_item.setAttribute("transform", 
					"translate(" + (item_posX + 285) + " " + item_posY + ")");
				//new_lamp.btn = createImageInput(SimpleLightBtnNormalDescription, SimpleLightBtnPressDescription, item_config.btn_addr, 74, 74);
				var ind = createImageOutput(
				{
					"w": 66,
					"h": 34,
					"img": "img/water/clapan_on.png"
				},
				{	
					"w": 66,
					"h": 34,
					"img": "img/water/clapan_off.png"
				},
				item_config.addr);
				new_item.container.appendChild(ind);
				appendDevArray(main_struct.outputArray, item_config.addr, ind);
				ind.off();
				ind.setAttribute("transform", "translate(-50 0)");
				//new_item.btn.setAttribute("transform", "translate(-50 0)");
				//new_item.appendChild(new_item.btn);
				
				wp.appendChild(new_item);
				item_posY += item_step;
				break;
				
			case "textout":
				TextOutWaterItem.child[0].child[0] = item_config.name;
				if(item_config.unline == true)
				{
					TextOutWaterItem.child[1].attr.display = "none";
				}
				else
				{
					TextOutWaterItem.child[1].attr.display = "block";
				}
				new_item = createMyElement(TextOutWaterItem);
				new_item.container = getElementByClass(new_item, "container");
				new_item.setAttribute("transform", 
					"translate(" + item_posX + " " + item_posY + ")");
									
				var text_out = createTextOutput(
				{
					"font-family": "Arial",
					"font-size": "35",
					"fill": "white",
					"y": "15",
					"x": "-10"
				},
				item_config.format,
				item_config.addr);
				appendDevArray(main_struct.analogOutArray, item_config.addr, text_out);
				new_item.container.appendChild(text_out);
				text_out.setPos(50, 0);
				text_out.set("123");
									
				wp.appendChild(new_item);
				item_posY += item_step;
				break;
		}
	}
	
	var some_btn = createImageInput(
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-43",
					"y": "-16",
					"width": "86",
					"height": "31",
					"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_off.png"]									
				}
			},
			{
				"name":	"text",
				"attr":
				{
					"x": "-18",
					"y": "4",
					"fill": "white",
					"font-family": "Arial",
					"font-weight": "bold",
					"size": "25"
				},
				"child":
				[
					"Вкл."
				]
			}
		]
	},					
	{
		"name": "g",
		"child":
		[
			{
				"name": "image",
				"attr":
				{
					"x": "-43",
					"y": "-16",
					"width": "86",
					"height": "31",
					"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_on.png"]									
				}
			},
			{
				"name":	"text",
				"attr":
				{
					"x": "-18",
					"y": "4",
					"fill": "black",
					"font-family": "Arial",
					"font-weight": "bold",
					"size": "25"
				},
				"child":
				[
					"Вкл."
				]
			}
		]
	}, 
	WaterConfigExt.btn_addr, 86, 31);
	some_btn.setAttribute("transform", "translate(700 575)");
	wp.appendChild(some_btn);	

	var btn_text = createMyElement(
	{
		"name": "text",
		"attr":
		{
			"font-family": "Arial",
			"font-size": 20,
			"fill": "white",
			"x": 750,
			"y": 580
		},
		"child":
		[
			WaterConfigExt.btn_text
		]
	});
	wp.appendChild(btn_text);	
};






