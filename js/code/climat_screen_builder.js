﻿
var PageMenuItem =
{
	"name": "g",
	"child":
	[
		{
			"name": "image",
			"attr":
			{
				"x": "-70",
				"y": "-17",
				"width": "140",
				"height": "33",
				"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_list.png"],
				"display": "none",
				"class": "select"
			}
		},
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-55",
				"y": "6",
				"class": "text"
			},
			"child":
			[
				"some text"
			]
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-70",
				"y": "-17",
				"width": "140",
				"height": "33",
				"fill": "red",
				"fill-opacity": "0",
				"class": "rect"
			}
		}
	]
};


var RoomItem = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-255",
				"y": "7"
			},
			"child":
			[
				"Dim lamp"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "25",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/light/horiz_line.png"]
			}
		},
		{
			"name": "image",
			"attr":
			{
				"x": "150",
				"y": "-14",
				"width": "113",
				"height": "28",
				"href": ["http://www.w3.org/1999/xlink", "img/light/-bg+.png"]
			}
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "red",
				"fill-opacity": "0"
			}
		}
	]
};

var RoomItem2 = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-255",
				"y": "32"
			},
			"child":
			[
				"Dim lamp"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "75",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/light/horiz_line.png"]
			}
		},
		{
			"name": "image",
			"attr":
			{
				"x": "150",
				"y": "-14",
				"width": "113",
				"height": "28",
				"href": ["http://www.w3.org/1999/xlink", "img/light/-bg+.png"]
			}
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "100",
				"fill": "red",
				"fill-opacity": "0"
			}
		}
	]
};

var RoomItem3 = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-255",
				"y": "7"
			},
			"child":
			[
				"Dim lamp"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "25",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/light/horiz_line.png"]
			}
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "red",
				"fill-opacity": "0"
			}
		}
	]
};

function build_climat_screen(climat_config)
{
	var wp = document.getElementById("climat_screen_wp2");
	wp.current_room = null;
	
	var room_index;
	var page_index;
	var new_item;

	var item_step = 42;
	var base = 384 - (climat_config.length * item_step)/2;
	var first_selected = null;
	for(page_index = 0; page_index < climat_config.length; page_index++)
	{
		PageMenuItem.child[1].child[0] = climat_config[page_index].name;
		new_item = createMyElement(PageMenuItem)
		new_item.setAttribute("transform", "translate(900 " + ( base + page_index*item_step ) + ")");
		new_item.select = getElementByClass(new_item, "select");
		new_item.text = getElementByClass(new_item, "text");
		new_item.container = createMyElement(
		{
			"name": "g", 
			"attr": 
			{
				"display": "none"
			}
		});
		//alert(new_item.container);
		new_item.rect = getElementByClass(new_item, "rect");
		new_item.rect.onclick = function()
		{
			var el = event.target.parentNode;
			if(wp.current_room != null)
			{
				if(wp.current_room == el)return;
				wp.current_room.text.setAttribute("fill", "white");
				wp.current_room.select.setAttribute("display", "none");
				wp.current_room.container.setAttribute("display", "none");
			}
			
			el.text.setAttribute("fill", "black");
			el.select.setAttribute("display", "block");
			el.container.setAttribute("display", "block");
			wp.current_room = el;
		};

		
		var room_posY = 200;
		var room_posX = 524;
		var room_step = 50;
		var new_room;
		for(room_index = 0; room_index < climat_config[page_index].objects.length; room_index++)
		{
			var room_config = climat_config[page_index].objects[room_index];
			
			switch(room_config.type)
			{
				case "th":
					var i
					for(i = 0; i < room_config.headers.length; i++)
					{
						var new_head = createMyElement(
						{
							"name": "g",
							"child":
							[
								{
									"name": "text",
									"attr":
									{
										"font-family": "Arial",
										"size": "15",
										"x": room_config.headers[i].x,
										"y": room_config.headers[i].y,
										"fill": "gainsboro"
									},
									"child":
									[
										room_config.headers[i].str,
									]
								}
							]
						});
					
						new_head.setAttribute("transform", 
							"translate(" + room_posX + " " + room_posY + ")");
							
						new_item.container.appendChild(new_head);					
					}
					break;
			
				case null:
				case "t1":
					RoomItem.child[0].child[0] = room_config.name;
					if(room_config.unline == true)
					{
						RoomItem.child[1].attr.display = "none";
					}
					else
					{
						RoomItem.child[1].attr.display = "block";
					}
					
					new_room = createMyElement(RoomItem);
					new_room.container = getElementByClass(new_room, "container");
					new_room.setAttribute("transform", 
						"translate(" + room_posX + " " + room_posY + ")");
					new_room.btn_plus = createImageInput(PlusLightBtnNormalDescription, PlusLightBtnPressDescription, 
															room_config.plus_addr, 35, 35);
					new_room.btn_plus.setAttribute("transform", "translate(256 2)");
					new_room.appendChild(new_room.btn_plus);
					new_room.btn_minus = createImageInput(MinusLightBtnNormalDescription, MinusLightBtnPressDescription, 
															room_config.minus_addr, 35, 35);
					new_room.btn_minus.setAttribute("transform", "translate(157 2)");
					new_room.appendChild(new_room.btn_minus);
					new_room.btn_sw = createImageInput(EmptyBtnNormalDescription, EmptyBtnPressDescription, 
															room_config.sw_addr, 86, 31);
					new_room.btn_sw.setAttribute("transform", "translate(40 2)");
					new_room.appendChild(new_room.btn_sw);
					
					new_room.curr_term = createTextOutput(
					{
						"font-family": "Arial",
						"font-size": "20",
						"fill": "white"
					}, "%DС" + String.fromCharCode(176), room_config.curr_addr);
					appendDevArray(main_struct.analogOutArray, room_config.curr_addr, new_room.curr_term);
					new_room.curr_term.setPos(-110, 7);
					new_room.curr_term.set("5");
					new_room.container.appendChild(new_room.curr_term);
					
					new_room.targ_term = createTextOutput(
					{
						"font-family": "Arial",
						"font-size": "20",
						"fill": "white"
					}, "%DС" + String.fromCharCode(176), room_config.targ_addr);
					appendDevArray(main_struct.analogOutArray, room_config.targ_addr, new_room.targ_term);
					new_room.targ_term.setPos(190, 7);
					new_room.targ_term.set("25");
					new_room.container.appendChild(new_room.targ_term);
					
					new_room.state_ind = createCombOutput(
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_on.png"]
								}
							},
							{
								"name": "text",
								"attr":
								{
									"font-family": "Arial",
									"font-size": "20",
									"fill": "black",
									"x": "-26",
									"y": "7"
								},
								"child":
								[
									"Откл."
								]
							}
						]
					},
					{				
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_off.png"]
								}
							},
							{
								"name": "text",
								"attr":
								{
									"font-family": "Arial",
									"font-size": "20",
									"fill": "gray",
									"x": "-20",
									"y": "7"
								},
								"child":
								[
									"Вкл."
								]
							}
						]
					}, room_config.state_addr);
					new_room.state_ind.off();
					new_room.btn_sw.ind.appendChild(new_room.state_ind);
					appendDevArray(main_struct.outputArray, room_config.state_addr, new_room.state_ind);
					
					new_item.container.appendChild(new_room);
					break;
					
				case "t2":
					RoomItem2.child[0].child[0] = room_config.name;
					if(room_config.unline == true)
					{
						RoomItem2.child[1].attr.display = "none";
					}
					else
					{
						RoomItem2.child[1].attr.display = "block";
					}
					
					new_room = createMyElement(RoomItem2);
					new_room.container = getElementByClass(new_room, "container");
					new_room.setAttribute("transform", 
						"translate(" + room_posX + " " + room_posY + ")");
					new_room.btn_plus = createImageInput(PlusLightBtnNormalDescription, PlusLightBtnPressDescription, 
															room_config.plus_addr, 35, 35);
					new_room.btn_plus.setAttribute("transform", "translate(256 2)");
					new_room.appendChild(new_room.btn_plus);
					new_room.btn_minus = createImageInput(MinusLightBtnNormalDescription, MinusLightBtnPressDescription, 
															room_config.minus_addr, 35, 35);
					new_room.btn_minus.setAttribute("transform", "translate(157 2)");
					new_room.appendChild(new_room.btn_minus);
					
					new_room.curr_term = createTextOutput(
					{
						"font-family": "Arial",
						"font-size": "20",
						"fill": "white"
					}, "%DС" + String.fromCharCode(176), room_config.curr_addr);
					appendDevArray(main_struct.analogOutArray, room_config.curr_addr, new_room.curr_term);
					new_room.curr_term.setPos(-110, 32);
					new_room.curr_term.set("5");
					new_room.container.appendChild(new_room.curr_term);
					
					new_room.targ_term = createTextOutput(
					{
						"font-family": "Arial",
						"font-size": "20",
						"fill": "white"
					}, "%DС" + String.fromCharCode(176), room_config.targ_addr);
					appendDevArray(main_struct.analogOutArray, room_config.targ_addr, new_room.targ_term);
					new_room.targ_term.setPos(190, 7);
					new_room.targ_term.set("25");
					new_room.container.appendChild(new_room.targ_term);

					new_room.btn_off = createImageInput(
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_off.png"]									
								}
							},
							{
								"name":	"text",
								"attr":
								{
									"x": "-24",
									"y": "4",
									"fill": "white",
									"font-family": "Arial",
									"font-weight": "bold",
									"size": "25"
								},
								"child":
								[
									"Выкл."
								]
							}
						]
					},					
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_on.png"]									
								}
							},
							{
								"name":	"text",
								"attr":
								{
									"x": "-24",
									"y": "4",
									"fill": "black",
									"font-family": "Arial",
									"font-weight": "bold",
									"size": "25"
								},
								"child":
								[
									"Выкл."
								]
							}
						]
					}, 
					room_config.off_addr, 86, 31);
					new_room.btn_off.setAttribute("transform", "translate(120 52)");
					new_room.appendChild(new_room.btn_off);
					
					new_room.btn_on = createImageInput(
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_off.png"]									
								}
							},
							{
								"name":	"text",
								"attr":
								{
									"x": "-18",
									"y": "4",
									"fill": "white",
									"font-family": "Arial",
									"font-weight": "bold",
									"size": "25"
								},
								"child":
								[
									"Вкл."
								]
							}
						]
					},					
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_on.png"]									
								}
							},
							{
								"name":	"text",
								"attr":
								{
									"x": "-18",
									"y": "4",
									"fill": "black",
									"font-family": "Arial",
									"font-weight": "bold",
									"size": "25"
								},
								"child":
								[
									"Вкл."
								]
							}
						]
					}, 
					room_config.on_addr, 86, 31);
					new_room.btn_on.setAttribute("transform", "translate(230 52)");
					new_room.appendChild(new_room.btn_on);
					
					new_room.btn_auto = createImageInput(
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_off.png"]									
								}
							},
							{
								"name":	"text",
								"attr":
								{
									"x": "-18",
									"y": "4",
									"fill": "white",
									"font-family": "Arial",
									"font-weight": "bold",
									"size": "25"
								},
								"child":
								[
									"Авто"
								]
							}
						]
					},					
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_on.png"]									
								}
							},
							{
								"name":	"text",
								"attr":
								{
									"x": "-18",
									"y": "4",
									"fill": "black",
									"font-family": "Arial",
									"font-weight": "bold",
									"size": "25"
								},
								"child":
								[
									"Авто"
								]
							}
						]
					}, 
					room_config.auto_addr, 86, 31);
					new_room.btn_auto.setAttribute("transform", "translate(10 52)");
					new_room.appendChild(new_room.btn_auto);

					new_room.ind = createMultiImageOutput(
					[
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/blue.png"
						},
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/green.png"
						},
						{
							"w": 49,
							"h": 49,
							"img": "img/alarm/red.png"
						},
						{
							"w": 50,
							"h": 49,
							"img": "img/alarm/gray.png"
						}
					], room_config.ind_addr);
					new_room.ind.set(0);
					new_room.ind.setAttribute("transform", "translate(10 0)");
					appendDevArray(main_struct.analogOutArray, room_config.ind_addr, new_room.ind);
					new_room.container.appendChild(new_room.ind);
					
					new_item.container.appendChild(new_room);
					room_posY += room_step;
					break;
					
				case "t3":
					RoomItem3.child[0].child[0] = room_config.name;
					if(room_config.unline == true)
					{
						RoomItem3.child[1].attr.display = "none";
					}
					else
					{
						RoomItem3.child[1].attr.display = "block";
					}
					new_room = createMyElement(RoomItem3);
					new_room.container = getElementByClass(new_room, "container");
					new_room.setAttribute("transform", 
						"translate(" + room_posX + " " + room_posY + ")");
						
					new_room.btn_off = createImageInput(
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_off.png"]									
								}
							},
							{
								"name":	"text",
								"attr":
								{
									"x": "-24",
									"y": "4",
									"fill": "white",
									"font-family": "Arial",
									"font-weight": "bold",
									"size": "25"
								},
								"child":
								[
									"Выкл."
								]
							}
						]
					},					
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_on.png"]									
								}
							},
							{
								"name":	"text",
								"attr":
								{
									"x": "-24",
									"y": "4",
									"fill": "black",
									"font-family": "Arial",
									"font-weight": "bold",
									"size": "25"
								},
								"child":
								[
									"Выкл."
								]
							}
						]
					}, 
					room_config.off_addr, 86, 31);
					new_room.btn_off.setAttribute("transform", "translate(100 2)");
					new_room.appendChild(new_room.btn_off);
					
					new_room.btn_on = createImageInput(
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_off.png"]									
								}
							},
							{
								"name":	"text",
								"attr":
								{
									"x": "-18",
									"y": "4",
									"fill": "white",
									"font-family": "Arial",
									"font-weight": "bold",
									"size": "25"
								},
								"child":
								[
									"Вкл."
								]
							}
						]
					},					
					{
						"name": "g",
						"child":
						[
							{
								"name": "image",
								"attr":
								{
									"x": "-43",
									"y": "-16",
									"width": "86",
									"height": "31",
									"href": ["http://www.w3.org/1999/xlink", "img/climat/btn_on.png"]									
								}
							},
							{
								"name":	"text",
								"attr":
								{
									"x": "-18",
									"y": "4",
									"fill": "black",
									"font-family": "Arial",
									"font-weight": "bold",
									"size": "25"
								},
								"child":
								[
									"Вкл."
								]
							}
						]
					}, 
					room_config.on_addr, 86, 31);
					new_room.btn_on.setAttribute("transform", "translate(210 2)");
					new_room.appendChild(new_room.btn_on);
					
					new_room.curr_term = createTextOutput(
					{
						"font-family": "Arial",
						"font-size": "20",
						"fill": "white"
					}, "%DС" + String.fromCharCode(176), room_config.curr_addr);
					appendDevArray(main_struct.analogOutArray, room_config.curr_addr, new_room.curr_term);
					new_room.curr_term.setPos(-110, 7);
					new_room.curr_term.set("5");
					new_room.container.appendChild(new_room.curr_term);
								
					new_room.ind = createMultiImageOutput(
					[
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/blue_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/green_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/red_small.png"
						},
						{
							"w": 25,
							"h": 25,
							"img": "img/alarm/gray_small.png"
						}
					], room_config.ind_addr);
					new_room.ind.set(0);
					new_room.ind.setAttribute("transform", "translate(-10 0)");
					appendDevArray(main_struct.analogOutArray, room_config.ind_addr, new_room.ind);
					new_room.container.appendChild(new_room.ind);
								
					new_item.container.appendChild(new_room);
					break;
			}
			
			room_posY += room_step;
		}
		
		wp.appendChild(new_item);
		wp.appendChild(new_item.container);
		if(page_index == 0)
		{
			first_selected = new_item;
		}
	}
	
	if(first_selected != null)
	{
		first_selected.text.setAttribute("fill", "black");
		first_selected.select.setAttribute("display", "block");
		first_selected.container.setAttribute("display", "block");
		wp.current_room = first_selected;
	}	
	
	var item_index;
	var item_posX = 133;
	var item_posY = 520;
	var item_step = 40;
	for(item_index = 0; item_index < ClimatSceneConfig.length; item_index++)
	{
		var scene_config = ClimatSceneConfig[item_index];
		//alert(scene_config.btn_addr);
		var scene_btn = createImageInput(
		{
			"name": "g"
		},
		{
			"name": "g"
		},
		scene_config.btn_addr, 190, 36);
		scene_btn.setPos(item_posX, item_posY + item_index*item_step);
		wp.appendChild(scene_btn);
		
		var scene_ind = createCombOutput(
		{
			"name": "g",
			"child":
			[
				{
					"name": "image",
					"attr":
					{
						"x": "-95",
						"y": "-18",
						"width": "190",
						"height": "36",
						"href": ["http://www.w3.org/1999/xlink", "img/light/btn_scene.png"]
					}
				},
				{
					"name": "text",
					"attr":
					{
						"font-family": "Arial",
						"font-size": "20",
						"fill": "black",
						"x": "-60",
						"y": "7"
					},
					"child":
					[
						scene_config.name
					]
				}
			]
		},
					{
			"name": "g",
			"child":
			[
				{
					"name": "text",
					"attr":
					{
						"font-family": "Arial",
						"font-size": "20",
						"fill": "white",
						"x": "-60",
						"y": "7"
					},
					"child":
					[
						scene_config.name
					]
				}
			]
		}, scene_config.ind_addr);
		scene_ind.off();
		appendDevArray(main_struct.outputArray, scene_config.ind_addr, scene_ind);
		scene_btn.ind.appendChild(scene_ind);
	}

};
