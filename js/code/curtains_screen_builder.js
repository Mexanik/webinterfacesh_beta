
var CurtainsItem = 
{
	"name": "g",
	"child":
	[
		{
			"name": "text",
			"attr":
			{
				"font-family": "Arial",
				"font-size": "18",
				"fill": "white",
				"x": "-245",
				"y": "7"
			},
			"child":
			[
				"TextOutWaterItem"
			]
		},
		{
			"name": "image",
			"attr":
			{
				"x": "-284",
				"y": "30",
				"width": "569",
				"height": "2",
				"href": ["http://www.w3.org/1999/xlink", "img/light/horiz_line.png"]
			}
		},
		{
			"name": "g",
			"attr":
			{
				"class": "container"
			}
		},
		{
			"name": "rect",
			"attr":
			{
				"x": "-285",
				"y": "-30",
				"width": "569",
				"height": "80",
				"fill": "red",
				"fill-opacity": "0"
			}
		}
	]
};

function build_curtains_screen(config)
{
	var wp = document.getElementById("curtains_screen_wp2");
		
	var item_index;

	var item_posY = 250;
	var item_posX = 550;
	var item_step = 60;
	var new_item;
	for(item_index = 0; item_index < config.length; item_index++)
	{
		var item_config = config[item_index];
		
		CurtainsItem.child[0].child[0] = item_config.name;
		if(item_config.unline == true)
		{
			CurtainsItem.child[1].attr.display = "none";
		}
		else
		{
			CurtainsItem.child[1].attr.display = "block";
		}
		new_item = createMyElement(CurtainsItem);
		new_item.container = getElementByClass(new_item, "container");
		new_item.setAttribute("transform", 
			"translate(" + item_posX + " " + item_posY + ")");

		var close = createImageInput(CloseBtnNormalDescription, CloseBtnPressDescription,
		item_config.close_addr, 100, 43);
		close.setPos(-10, 0);
		new_item.appendChild(close);
		
		var minus = createImageInput(MinusLightBtnNormalDescription, MinusLightBtnPressDescription,
		item_config.minus_addr, 35, 35);
		minus.setPos(70, 0);
		new_item.appendChild(minus);
		
		var plus = createImageInput(PlusLightBtnNormalDescription, PlusLightBtnPressDescription,
		item_config.plus_addr, 35, 35);
		plus.setPos(130, 0);
		new_item.appendChild(plus);
							
		var open = createImageInput(OpenBtnNormalDescription, OpenBtnPressDescription,
		item_config.open_addr, 100, 43);
		open.setPos(210, 0);
		new_item.appendChild(open);
		
		wp.appendChild(new_item);
		item_posY += item_step;
	}
					
};



